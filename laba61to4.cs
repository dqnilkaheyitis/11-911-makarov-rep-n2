﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MaccuB
{
    class Program
    {

        static void Print(int [] russian_weight)
        {
            for (int i = 0; i < russian_weight.Length; i++)
            {
                Console.Write(russian_weight[i] + " ");
            }
        }

        static void Print(double [] second)
        {
            for (int i = 0; i < second.Length; i++)
            {
                Console.Write(" " + second[i]);
            }
        }

        static void Main(string[] args)
        {
            Console.WriteLine("Задание 1.");
            Random random = new Random();
            int i; 
            int[] russia_weight = new int[20];
            for (i = 0; i < russia_weight.Length; i++)
            {
                russia_weight[i] = random.Next(50, 100);
                Console.WriteLine("Для {0}-го человека вес = {1}", i+1, russia_weight[i]);
            }
            Console.WriteLine();

            Console.WriteLine("Задание 2.");
            Random random1 = new Random();
            double [] second = new double [10];
            for (i = 0; i < second.Length; i++)
            {
                second[i] = random1.Next(1, 10);
            }
                Console.WriteLine("Массив до умножения на два:");
            Print(second);
            Console.WriteLine();
                for (i = 0; i < second.Length; i++)
                { 
                    second[i] = second[i] * 2;
                }
            Console.WriteLine("Массив после умножения на два:");
            Print(second);
            Console.WriteLine();
            double k;
            for (i = 0; i < 1; i++)
            {
                k = second[i];
                Console.WriteLine(" Первый элемент k = {0}", k);
                for (i = 0; i < second.Length; i++)
                {
                   Convert.ToDouble(second[i] = second[i] / k);
                }
            }
            Console.WriteLine("Массив после деления на первый элемент: ");
            Print(second);
            Console.WriteLine();
            Console.WriteLine("Введите значние для А: ");
            int A = int.Parse(Console.ReadLine());
            for (i = 0; i < second.Length; i++)
            {
                second[i] = second[i] - A;
            }
            Print(second);
            Console.WriteLine();
            Console.WriteLine();

            Console.WriteLine("Задание 3.");
            Random random2 = new Random();
            int[] third = new int[10];
            for (i = 0; i < third.Length; i++)
            {
                third[i] = random2.Next(5, 15);
            }
            int c = 0;
            for (i = 0; i < third.Length; i++)
            {
                c = c + third[i];
            }
            if (c % 2 == 0)
                Console.WriteLine("C = {0}. Сумма элементов массива - чётна.", c);
            else
                Console.WriteLine("C = {0}. Сумма элементов массива - нечётна.", c);
            c = 0;
            for (i = 0; i < third.Length; i++)
            {
                c = c + (int)Math.Pow(third[i], 2);
            }
            Console.WriteLine();
            if (c > 9999 && c < 100000)
                Console.WriteLine("C^2 = {0}. Число суммы квадратов массива пятизначно.", c);
            else
                Console.WriteLine("C^2 = {0}. Число суммы квадратов не пятизначно.", c);
            Console.WriteLine();
            Console.WriteLine();

            Console.WriteLine("Задание 4.");
            Console.WriteLine("Введите количество домов: ");
            Console.WriteLine();
            int l = int.Parse(Console.ReadLine());
            int f = 0;
            int fne = 0;
            int[] fours = new int[l];
            for (i = 0; i < fours.Length; i++)
            {
                fours[i] = i + 1;
            }
            for (i=0; i<fours.Length; i++)
            {
                if (fours[i] % 2 == 0)
                    f += fours[i];
                else
                    fne += fours[i];
            }
            Console.WriteLine("На чётной стороне проживает - {0}", f);
            Console.WriteLine("На нечётной стороне проиживает - {0}", fne);
            if (f > fne)
                Console.WriteLine("Больше людей проживает на чётной стороне.");
            else
                Console.WriteLine("Больше людей проживает на нечётной стороне");

            Console.ReadKey();
        }

    }
}
