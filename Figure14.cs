﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace oop3
{
    abstract class Figure
    {
        abstract public double S();
        abstract public double P();
        abstract public string FigureInfo();
        abstract public string Perm();
    }
}
