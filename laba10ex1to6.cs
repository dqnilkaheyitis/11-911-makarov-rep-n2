﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Text.RegularExpressions;
namespace regular
{
    class Example
    {
        static void fst(string text, string pattern)
        {
            if (Regex.IsMatch(text, pattern))
            {
                Console.WriteLine("Найдено слово - " + pattern);
            }
            else
            {
                Console.WriteLine("Не найдёно");
            }
        }

        static void sec(int x, string text)
        {
            string pattern = @"\b\w{" + x.ToString() +@"}\b";
            foreach (Match match in Regex.Matches(text, pattern))
                Console.WriteLine("Слово заданное длины - " + match.Value);
        }

        static void third(string text)
        {
            Regex regex = new Regex(@"(^\s|[А-Я])(\w*)");
            MatchCollection matches = regex.Matches(text);
            if (matches.Count > 0)
            {
                foreach (Match bob in matches)
                    Console.WriteLine("Word - " + bob.Value);
            }
            else
                Console.WriteLine("Таких слов не существует.");
        }

        static void four(ref string text)
        {
            Regex regex = new Regex(@" *(\,|\.|\!|\?|\:|\;|\""|\-|\.+)");
            string result = regex.Replace(text, string.Empty);
            Console.Write("Строка, после удаления всех знаков препинания - " + result);
            Console.WriteLine();
            text = result;
        }

        static void five (ref string text)
        {
            Regex regex = new Regex(@"\b[A-Z]\w*\b|[a-z]\w*\b");
            string y = "...";
            string result = regex.Replace(text, y);
            Console.Write("Строка, после замены всех английских слов на многоточие - " + result);
            Console.WriteLine();
            text = result;
        }

        static void six(string text)
        {
            int b = 0;
            int c = 0;
            Regex regex1 = new Regex(@"\b\D*\b");
            text = regex1.Replace(text, string.Empty);
            Regex regex = new Regex(@"\d+");
            MatchCollection match = regex.Matches(text);
            if (match.Count > 0)
            {
                foreach (Match bob in match)
                {
                    b = int.Parse(bob.Value);
                    c = c + b;
                }
            }
            Console.WriteLine("Сумма всех чисел, входящих в строку - " + c);
        }
        static void Main(string[] args)
        {
            
            

            Console.WriteLine("Задание 1.");
            Console.Write("Введите текст: ");
            string text = Console.ReadLine();
            Console.Write("Введите слово, которое нужно проверить, находится ли оно в тексте: ");
            string pattern = Console.ReadLine();
            fst(text, pattern);
            Console.WriteLine();


            Console.WriteLine("Задание 2.");
            Console.Write("Введите необходимую длинну слова: ");
            int x = int.Parse(Console.ReadLine());
            sec(x, text);
            Console.WriteLine();


            Console.WriteLine("Задание 3.");
            third(text);
            Console.WriteLine();


            Console.WriteLine("Задание 4.");
            four(ref text);
            Console.WriteLine();


            Console.WriteLine("Задание 5.");
            five(ref text);
            Console.WriteLine();


            Console.WriteLine("Задание 6.");
            six(text);

            Console.ReadKey();
        }
    }
    
}
