﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SecondPart
{
    class RacionalFraction
    {
        public double x;
        public double y;

        public RacionalFraction(double x, double y)
        {
            this.x = x;
            this.y = y;
        }

        public RacionalFraction()
        {

        }
        public void reduce()
        {
            double x1 = 0;
            double y1 = 0;
            for (int i = 1; i <= 100000; i++)
            {
                if (x % i == 0 & y % i == 0)
                {
                    x1 = x / i;
                    y1 = y / i;
                }
            }
            x = x1;
            y = y1;
            Console.WriteLine("Сокращенная дробь - {0}/{1}", x1, y1);
        }
        public void reduce(ref double x, ref double y)
        {
            double x1 = 0;
            double y1 = 0;
            for (int i = 1; i <= 100000; i++)
            {
                if (x % i == 0 & y % i == 0)
                {
                    x1 = x / i;
                    y1 = y / i;
                }
            }
            x = x1;
            y = y1;
        }


        public RacionalFraction add(RacionalFraction obj)
        {

            if (obj.y == y)
            {
                x = x + obj.x;
            }
            else
            {
                double y2 = obj.y;
                double y2_2 = y;
                double x2 = obj.x;
                double x2_2 = x;
                obj.y = y2_2 * y2;
                y = obj.y;
                x = x2_2 * y2;
                obj.x = x2 * y2_2;
                x = x + obj.x;
            }
            reduce(ref x, ref y);
            Console.WriteLine("Сумма двух дробей - {0}/{1}", x, y);
            RacionalFraction a = new RacionalFraction(x, y);
            return a;
        }

        public void add2(RacionalFraction obj)
        {
            if (obj.y == y)
            {
                x = x + obj.x;
            }
            else
            {
                double y2 = obj.y;
                double y2_2 = y;
                double x2 = obj.x;
                double x2_2 = x;
                obj.y = y2_2 * y2;
                y = obj.y;
                x = x2_2 * y2;
                obj.x = x2 * y2_2;
                x = x + obj.x;
            }
            reduce(ref x, ref y);
            Console.WriteLine("Сумма двух дробей - {0}/{1}", x, y);
        }

        public RacionalFraction sub(RacionalFraction obj)
        {
            RacionalFraction b = new RacionalFraction(x, y);
            double y2 = obj.y;
            double y2_2 = y;
            double x2 = obj.x;
            double x2_2 = x;
            double s = x - obj.x;
            double s1 = y;
            reduce(ref s, ref s1);
            Console.WriteLine("Вычитание дробей - {0}/{1}", s, s1);
            RacionalFraction c = new RacionalFraction(s, s1);
            return c;
        }

        public void sub1(RacionalFraction obj)
        {
            double y2 = obj.y;
            double y2_2 = this.y;
            double x2 = obj.x;
            double x2_2 = this.x;
            x = x - obj.x;
            reduce(ref x, ref y);
            Console.WriteLine("Вычитание дробей - {0}/{1}", x, y);
        }

        public RacionalFraction multi(RacionalFraction obj)
        {
            reduce(ref x, ref y);
            double s = x*obj.x;
            double s1 = y * obj.y;
            reduce(ref s, ref s1);
            Console.WriteLine("Дробь после переумножения - {0}/{1}",s,s1);
            RacionalFraction f = new RacionalFraction(s,s1);
            return f;
        }

        public void multi1(RacionalFraction obj)
        {
            x = x * obj.x;
            y = y * obj.y;
            reduce(ref x, ref y);
            Console.WriteLine("Дробь после переумножения - {0}/{1}", x, y);
        }

        public RacionalFraction div (RacionalFraction obj)
        {
            double s = x * obj.y;
            double s1 = y * obj.x;
            reduce(ref s, ref s1);
            Console.WriteLine("Деление дробей - {0}/{1}", s, s1);
            RacionalFraction v = new RacionalFraction(s, s1);
            return v;
        }
        public void div1(RacionalFraction obj)
        {
            x = x * obj.y;
            y = y * obj.x;
            reduce(ref x, ref y);
            Console.WriteLine("Деление дробей - {0}/{1}", x, y);
        }

        public override string ToString()
        {
            string s = "(" + x + " / " + y + ")";
            return s;
        }

        public double value()
        {
            double z = x / y;
            return z;
        }

        public bool equals(RacionalFraction obj)
        {
            Console.WriteLine("Первая дробь - {0}:{1}", x, y);
            Console.WriteLine("Вторая дробь - {0}:{1}", obj.x, obj.y);
            if (x / y > obj.x / obj.y)
                return true;
            else
                return false;
        }

        public int numberPart()
        {
            x = Math.Round(x / y);
            return Convert.ToInt32(x);
        }
    }
}
