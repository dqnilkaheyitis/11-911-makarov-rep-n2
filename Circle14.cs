﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace oop3
{
    class Circle : Figure
    {
        public double r;
        public Circle()
        {

        }

        public Circle(double r)
        {
            this.r = r;
        }

        public override double S()
        {
            return Math.PI * Math.Pow(r,2);
        }

        public override double P()
        {
            return Math.PI * 2 * r;
        }

        public override string FigureInfo()
        {
            string s = "S: " + Convert.ToString(S() + " P: " + Convert.ToString(P()));
            return s;
        }
        public override string Perm()
        {
            string s = "Радиус круга: " + " " + Convert.ToString(r);
            return s;
        }
    }
}
