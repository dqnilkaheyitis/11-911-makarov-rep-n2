﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Lesson_3
{
    class Program
    {
        static void Main(string[] args)
        {
            //Первый блок заданий.

            //Num 1
            //Способ while.
            Console.WriteLine("Задание 1 (while)");
            Console.WriteLine();
            int i = 10;
            while (i < 60)
                i += 2;
            if (i == 60)
                while (i >= 10)
                {
                    Console.WriteLine((i) + " ");
                    i = i - 2;
                }

            //Способ for.
            Console.WriteLine("Задание 1 (for)");
            Console.WriteLine();
            for (i = 60; i >= 10; i = i - 2)
            {
                Console.WriteLine(i);
            }

            //Способ do while.
            Console.WriteLine("Задание 1 (while do)");
            Console.WriteLine();
            i = 62;
            do
            {
                Console.WriteLine(i = i - 2);
            }
            while (i > 10);


            //Num 2 
            //Способ while.
            Console.WriteLine();
            Console.WriteLine();
            Console.WriteLine("Задание 2 (while)");
            int a = 1;
            double b = 1;
            Console.WriteLine();
            Console.WriteLine("КИЛЛОГРАММ = ФУНТ");
            Console.WriteLine();
            while (a <= 10)
            {
                Console.Write(a + " " + "килограмм" + " " + "равен" + " " + (b = a * 2.20462) + " " + "фунтов."); ;
                Console.WriteLine();
                a = a + 1;
            }
            Console.WriteLine();
            Console.WriteLine("ФУНТ = КИЛОГРАММ");
            Console.WriteLine();
            a = 1;
            while (a < 10)
            {
                Console.Write(a + " " + "фунт" + " " + "равен" + " " + (b = a * 0.453592) + " " + "килограмм"); ;
                Console.WriteLine();
                a = a + 1;

            }

            //Способ for.
            Console.WriteLine();
            Console.WriteLine();
            Console.WriteLine("Задание 2 (for)");

            b = 1;
            Console.WriteLine();
            Console.WriteLine("КИЛЛОГРАММ = ФУНТ");
            Console.WriteLine();
            for (a = 1; a <= 10; a++)
            {
                Console.Write(a + " " + "килограмм" + " " + "равен" + " " + (b = a * 2.20462) + " " + "фунтов."); ;
                Console.WriteLine();
            }
            Console.WriteLine();
            Console.WriteLine("ФУНТ = КИЛОГРАММ");
            Console.WriteLine();
            for (a = 1; a <= 10; a++)
            {
                Console.Write(a + " " + "фунт" + " " + "равен" + " " + (b = a * 0.453592) + " " + "килограмм"); ;
                Console.WriteLine();

            }

            //Способ do while.
            Console.WriteLine();
            Console.WriteLine();
            Console.WriteLine("Задание 2 (do while)");
            a = 1;
            b = 1;
            Console.WriteLine();
            Console.WriteLine("КИЛЛОГРАММ = ФУНТ");
            Console.WriteLine();
            do
            {
                Console.Write(a + " " + "килограмм" + " " + "равен" + " " + (b = a * 2.20462) + " " + "фунтов."); ;
                Console.WriteLine();
                a = a + 1;
            }
            while (a <= 10);
            Console.WriteLine();
            Console.WriteLine("ФУНТ = КИЛОГРАММ");
            Console.WriteLine();
            a = 1;
            do
            {
                Console.Write(a + " " + "фунт" + " " + "равен" + " " + (b = a * 0.453592) + " " + "килограмм"); ;
                Console.WriteLine();
                a = a + 1;
            }
            while (a <= 10);



            //Num 3
            //Способ while.
            Console.WriteLine();
            Console.WriteLine();
            Console.WriteLine("Задание 3 (while)");
            Console.WriteLine();
            Console.Write("Введите начальное число диапазона: ");
            a = int.Parse(Console.ReadLine());
            Console.WriteLine();
            Console.Write("Введите конечное число диапазона: ");
            b = int.Parse(Console.ReadLine());
            Console.WriteLine();
            Console.WriteLine("Кубы последовальных чисел диапазона [{0};{1}]: ", a, b);
            Console.WriteLine();
            while (a <= b)
            {
                Console.WriteLine(Math.Pow(a, 3));
                a = a + 1;
                Console.WriteLine();
            }
            Console.Write("Введите начальное число диапазона для обртаного порядка: ");
            a = int.Parse(Console.ReadLine());
            Console.WriteLine();
            Console.Write("Введите конечное число диапазона для обртаного порядка: ");
            b = int.Parse(Console.ReadLine());
            Console.WriteLine();
            Console.WriteLine("Кубы последовальных чисел диапазона в обратном порядке диапазона [{0};{1}]: ", a, b);
            Console.WriteLine();
            while (b >= a)
            {
                Console.WriteLine(Math.Pow(b, 3));
                b = b - 1;
                Console.WriteLine();
            }

            //Способ for.
            Console.WriteLine();
            Console.WriteLine();
            Console.WriteLine("Задание 3 (for)");
            for (Console.Write("Введите начальное число диапазона: "),
            a = int.Parse(Console.ReadLine()),
            Console.WriteLine(), Console.Write("Введите конечное число диапазона: "),
            b = int.Parse(Console.ReadLine()),
            Console.WriteLine(), Console.WriteLine("Кубы последовальных чисел диапазона [{0};{1}]: ", a, b),
            Console.WriteLine(); a <= b; a = a + 1)
            {
                Console.WriteLine(Math.Pow(a, 3));
                Console.WriteLine();
            }
            for (Console.Write("Введите начальное число диапазона: "),
            a = int.Parse(Console.ReadLine()),
            Console.WriteLine(), Console.Write("Введите конечное число диапазона: "),
            b = int.Parse(Console.ReadLine()),
            Console.WriteLine(), Console.WriteLine("Кубы последовальных чисел диапазона в обратном порядке диапазона [{0};{1}]: ", a, b),
            Console.WriteLine(); a <= b; b = b - 1)
            {
                Console.WriteLine(Math.Pow(b, 3));
                Console.WriteLine();
            }

            //Способ do while.
            Console.WriteLine();
            Console.WriteLine();
            Console.WriteLine("Задание 3 (do while)");
            Console.WriteLine();
            Console.Write("Введите начальное число диапазона: ");
            a = int.Parse(Console.ReadLine());
            Console.WriteLine();
            Console.Write("Введите конечное число диапазона: ");
            b = int.Parse(Console.ReadLine());
            Console.WriteLine();
            Console.WriteLine("Кубы последовальных чисел диапазона [{0};{1}]: ", a, b);
            Console.WriteLine();
            do
            {
                Console.WriteLine(Math.Pow(a, 3));
                a = a + 1;
                Console.WriteLine();
            }
            while (a <= b);
            Console.Write("Введите начальное число диапазона для обртаного порядка: ");
            a = int.Parse(Console.ReadLine());
            Console.WriteLine();
            Console.Write("Введите конечное число диапазона для обртаного порядка: ");
            b = int.Parse(Console.ReadLine());
            Console.WriteLine();
            Console.WriteLine("Кубы последовальных чисел диапазона в обратном порядке диапазона [{0};{1}]: ", a, b);
            Console.WriteLine();
            do
            {
                Console.WriteLine(Math.Pow(b, 3));
                b = b - 1;
                Console.WriteLine();
            }
            while (b >= a);

            //Num 4 
            //Способ while.
            Console.WriteLine();
            Console.WriteLine();
            Console.WriteLine("Задание 4 (while)");
            Console.WriteLine();
            Console.Write("Введите начальное число диапазона: ");
            a = int.Parse(Console.ReadLine());
            Console.WriteLine();
            Console.Write("Введите конечное число диапазона: ");
            b = int.Parse(Console.ReadLine());
            Console.WriteLine();
            Console.Write("Введите цифру, числа оканчивающие на которую будут выводиться на экран, из заданного вами диапазона [{0},{1}]: ", a, b);
            int x = int.Parse(Console.ReadLine());
            Console.WriteLine();
            while (a <= b)
            {
                if (a % 10 == x)
                {
                    Console.WriteLine(a);
                    Console.WriteLine();
                }
                a = a + 1;
            }

            //Способ for.
            Console.WriteLine();
            Console.WriteLine();
            Console.WriteLine("Задание 4 (for)");
            Console.WriteLine();
            for (Console.Write("Введите начальное число диапазона: "),
            a = int.Parse(Console.ReadLine()),
            Console.WriteLine(),
            Console.Write("Введите конечное число диапазона: "),
            b = int.Parse(Console.ReadLine()),
            Console.WriteLine(),
            Console.Write("Введите цифру, числа оканчивающие на которую будут выводиться на экран, из заданного вами диапазона [{0},{1}]: ", a, b),
            x = int.Parse(Console.ReadLine()),
            Console.WriteLine(); a <= b; a = a + 1)
            {
                if (a % 10 == x)
                {
                    Console.WriteLine(a);
                    Console.WriteLine();
                }
            }

            //Способ do while.
            Console.WriteLine();
            Console.WriteLine();
            Console.WriteLine("Задание 4 (do while)");
            Console.WriteLine();
            Console.Write("Введите начальное число диапазона: ");
            a = int.Parse(Console.ReadLine());
            Console.WriteLine();
            Console.Write("Введите конечное число диапазона: ");
            b = int.Parse(Console.ReadLine());
            Console.WriteLine();
            Console.Write("Введите цифру, числа оканчивающие на которую будут выводиться на экран, из заданного вами диапазона [{0},{1}]: ", a, b);
            x = int.Parse(Console.ReadLine());
            Console.WriteLine();
            do
            {
                if (a % 10 == x)
                {
                    Console.WriteLine(a);
                    Console.WriteLine();
                }
                a = a + 1;
            }
            while (a <= b);


            //Num 5
            //Способ while.
            Console.WriteLine();
            Console.WriteLine();
            Console.WriteLine("Задание 5 (while)");
            Console.WriteLine();
            Console.Write("Введите начальное число диапазона: ");
            a = int.Parse(Console.ReadLine());
            Console.WriteLine();
            Console.Write("Введите конечное число диапазона: ");
            b = int.Parse(Console.ReadLine());
            Console.WriteLine();
            while (a <= b)
            {
                if (a >= 0 && (Convert.ToInt32(a) == Convert.ToDouble(a)))

                {
                    Console.WriteLine(a);
                    Console.WriteLine();
                }
                a = a + 1;
            }

            //Способ for.
            Console.WriteLine();
            Console.WriteLine();
            Console.WriteLine("Задание 5 (for)");
            Console.WriteLine();
            for (Console.Write("Введите начальное число диапазона: "),
            a = int.Parse(Console.ReadLine()),
            Console.WriteLine(),
            Console.Write("Введите конечное число диапазона: "),
            b = int.Parse(Console.ReadLine()),
            Console.WriteLine(); a <= b; a = a + 1)
            {
                if (a >= 0 && (Convert.ToInt32(a) == Convert.ToDouble(a)))
                {
                    Console.WriteLine(a);
                    Console.WriteLine();
                }
            }

            //Способ do while.
            Console.WriteLine();
            Console.WriteLine();
            Console.WriteLine("Задание 5 (do while)");
            Console.WriteLine();
            Console.Write("Введите начальное число диапазона: ");
            a = int.Parse(Console.ReadLine());
            Console.WriteLine();
            Console.Write("Введите конечное число диапазона: ");
            b = int.Parse(Console.ReadLine());
            Console.WriteLine();
            do
            {
                if (a >= 0 && (Convert.ToInt32(a) == Convert.ToDouble(a)))

                {
                    Console.WriteLine(a);
                    Console.WriteLine();
                }
                a = a + 1;
            }
            while (a <= b);


            //Num 6
            //Способ while.
            Console.WriteLine();
            Console.WriteLine();
            Console.WriteLine("Задание 6 (while)");
            Console.WriteLine();
            Console.Write("Введите начальное число диапазона: ");
            a = int.Parse(Console.ReadLine());
            Console.WriteLine();
            Console.Write("Введите конечное число диапазона: ");
            b = int.Parse(Console.ReadLine());
            Console.WriteLine();
            while (a <= b)
            {
                if (a % 3 == 0)
                {
                    Console.WriteLine(a);
                    Console.WriteLine();
                }
                a = a + 1;
            }

            //Способ for.
            Console.WriteLine();
            Console.WriteLine();
            Console.WriteLine("Задание 6 (for)");
            Console.WriteLine();
            for (Console.Write("Введите начальное число диапазона: "),
            a = int.Parse(Console.ReadLine()),
            Console.WriteLine(),
            Console.Write("Введите конечное число диапазона: "),
            b = int.Parse(Console.ReadLine()),
            Console.WriteLine(); a <= b; a = a + 1)
            {
                if (a % 3 == 0)
                {
                    Console.WriteLine(a);
                    Console.WriteLine();
                }
            }

            //Способ do while.
            Console.WriteLine();
            Console.WriteLine();
            Console.WriteLine("Задание 6 (do while)");
            Console.WriteLine();
            Console.Write("Введите начальное число диапазона: ");
            a = int.Parse(Console.ReadLine());
            Console.WriteLine();
            Console.Write("Введите конечное число диапазона: ");
            b = int.Parse(Console.ReadLine());
            Console.WriteLine();
            do
            {
                if (a % 3 == 0)
                {
                    Console.WriteLine(a);
                    Console.WriteLine();
                }
                a = a + 1;
            }
            while (a <= b);


            //Num 7 
            //Способ while.
            Console.WriteLine();
            Console.WriteLine();
            Console.WriteLine("Задание 7 (while)");
            Console.WriteLine();
            Console.Write("Введите начальное число диапазона: ");
            a = int.Parse(Console.ReadLine());
            Console.WriteLine();
            Console.Write("Введите конечное число диапазона: ");
            b = int.Parse(Console.ReadLine());
            Console.WriteLine();
            int x1 = 1;
            int x2 = 1;
            while (a <= b)
            {
                x1 = a % 10;
                x2 = (a / 10) % 10;
                if (x1 != x2)
                {
                    if (a > 9 & a < 100)
                    {
                        Console.WriteLine(a);
                        Console.WriteLine();
                    }
                }
                a = a + 1;
            }

            //Способ for.
            Console.WriteLine();
            Console.WriteLine();
            Console.WriteLine("Задание 7 (for)");
            Console.WriteLine();
            for (Console.Write("Введите начальное число диапазона: "),
            a = int.Parse(Console.ReadLine()),
            Console.WriteLine(),
            Console.Write("Введите конечное число диапазона: "),
            b = int.Parse(Console.ReadLine()),
            Console.WriteLine(),
            x1 = 1,
            x2 = 1; a <= b; a = a + 1)
            {
                x1 = a % 10;
                x2 = (a / 10) % 10;
                if (x1 != x2)
                {
                    if (a > 9 & a < 100)
                    {
                        Console.WriteLine(a);
                        Console.WriteLine();
                    }
                }
            }

            //Способ do while.
            Console.WriteLine();
            Console.WriteLine();
            Console.WriteLine("Задание 7 (do while)");
            Console.WriteLine();
            Console.Write("Введите начальное число диапазона: ");
            a = int.Parse(Console.ReadLine());
            Console.WriteLine();
            Console.Write("Введите конечное число диапазона: ");
            b = int.Parse(Console.ReadLine());
            Console.WriteLine();
            x1 = 1;
            x2 = 1;
            do
            {
                x1 = a % 10;
                x2 = (a / 10) % 10;
                if (x1 != x2)
                {
                    if (a > 9 & a < 100)
                    {
                        Console.WriteLine(a);
                        Console.WriteLine();
                    }
                }
                a = a + 1;
            }
            while (a <= b);



            //Num 8 
            //Способ while.
            Console.WriteLine();
            Console.WriteLine();
            Console.WriteLine("Задание 8 (while)");
            Console.WriteLine();
            Console.Write("Введите начальное число диапазона: ");
            a = int.Parse(Console.ReadLine());
            Console.WriteLine();
            Console.Write("Введите конечное число диапазона: ");
            b = int.Parse(Console.ReadLine());
            Console.WriteLine();
            x1 = 1;
            x2 = 1;
            while (a <= b)
            {
                x1 = a % 10;
                x2 = (a / 10) % 10;
                if (x2 > x1 && (x2 - x1) <= 1)
                {
                    if (a > 9 & a < 100)
                    {
                        Console.WriteLine(a);
                        Console.WriteLine();
                    }
                }
                a = a + 1;
            }

            //Способ for.
            Console.WriteLine();
            Console.WriteLine();
            Console.WriteLine("Задание 8 (for)");
            Console.WriteLine();
            for (Console.Write("Введите начальное число диапазона: "),
            a = int.Parse(Console.ReadLine()),
            Console.WriteLine(),
            Console.Write("Введите конечное число диапазона: "),
            b = int.Parse(Console.ReadLine()),
            Console.WriteLine(),
            x1 = 1,
            x2 = 1; a <= b; a = a + 1)
            {
                x1 = a % 10;
                x2 = (a / 10) % 10;
                if (x2 > x1 && (x2 - x1) <= 1)
                {
                    if (a > 9 & a < 100)
                    {
                        Console.WriteLine(a);
                        Console.WriteLine();
                    }
                }
            }

            //Способ do while.
            Console.WriteLine();
            Console.WriteLine();
            Console.WriteLine("Задание 8 (do while)");
            Console.WriteLine();
            Console.Write("Введите начальное число диапазона: ");
            a = int.Parse(Console.ReadLine());
            Console.WriteLine();
            Console.Write("Введите конечное число диапазона: ");
            b = int.Parse(Console.ReadLine());
            Console.WriteLine();
            x1 = 1;
            x2 = 1;
            do
            {
                x1 = a % 10;
                x2 = (a / 10) % 10;
                if (x2 > x1 && (x2 - x1) <= 1)
                {
                    if (a > 9 & a < 100)
                    {
                        Console.WriteLine(a);
                        Console.WriteLine();
                    }
                }
                a = a + 1;
            }
            while (a <= b);

            //Num 9 
            //Способ while.
            Console.WriteLine();
            Console.WriteLine();
            Console.WriteLine("Задание 9 (while)");
            Console.WriteLine();
            Console.Write("Введите начальное число диапазона: ");
            a = int.Parse(Console.ReadLine());
            Console.WriteLine();
            Console.Write("Введите конечное число диапазона: ");
            b = int.Parse(Console.ReadLine());
            Console.WriteLine();
            x1 = 1;
            x2 = 1;
            int x3 = 1;
            while (a <= b)
            {
                x1 = a % 10;
                x2 = (a / 10) % 10;
                x3 = (a / 100) % 10;
                if ((x1 == x2 && x1 == x3) || (x2 == x3) || (x3 == x1) || (x1 == x2))
                {
                    if (a > 99 & a < 1000)
                    {
                        Console.WriteLine(a);
                        Console.WriteLine();
                    }
                }
                a = a + 1;
            }

            //Способ for.
            Console.WriteLine();
            Console.WriteLine();
            Console.WriteLine("Задание 9 (for)");
            Console.WriteLine();
            for (Console.Write("Введите начальное число диапазона: "),
            a = int.Parse(Console.ReadLine()),
            Console.WriteLine(),
            Console.Write("Введите конечное число диапазона: "),
            b = int.Parse(Console.ReadLine()),
            Console.WriteLine(),
            x1 = 1,
            x2 = 1,
            x3 = 1; a <= b; a = a + 1)
            {
                x1 = a % 10;
                x2 = (a / 10) % 10;
                x3 = (a / 100) % 10;
                if ((x1 == x2 && x1 == x3) || (x2 == x3) || (x3 == x1) || (x1 == x2))
                {
                    if (a > 99 & a < 1000)
                    {
                        Console.WriteLine(a);
                        Console.WriteLine();
                    }
                }
            }

            //Способ do while.
            Console.WriteLine();
            Console.WriteLine();
            Console.WriteLine("Задание 9 (do while)");
            Console.WriteLine();
            Console.Write("Введите начальное число диапазона: ");
            a = int.Parse(Console.ReadLine());
            Console.WriteLine();
            Console.Write("Введите конечное число диапазона: ");
            b = int.Parse(Console.ReadLine());
            Console.WriteLine();
            x1 = 1;
            x2 = 1;
            x3 = 1;
            do
            {
                x1 = a % 10;
                x2 = (a / 10) % 10;
                x3 = (a / 100) % 10;
                if ((x1 == x2 && x1 == x3) || (x2 == x3) || (x3 == x1) || (x1 == x2))
                {
                    if (a > 99 & a < 1000)
                    {
                        Console.WriteLine(a);
                        Console.WriteLine();
                    }
                }
                a = a + 1;
            }
            while (a <= b);

            //Второй блок заданий.

           


            //Num 2 (т.к первым номером был обозначен пример)

            Console.WriteLine();
            Console.WriteLine();
            Console.WriteLine("Задание 2");
            i = 1;
            int j = 0;
            while (i <= 10 && j <= 4)
            {
                Console.Write(i + " ");
                i = i + 1;
                if (i == 11)
                {
                    j = j + 1;
                    i = 1;
                    Console.WriteLine();
                }
            }

            //Num 3

            Console.WriteLine();
            Console.WriteLine();
            Console.WriteLine("Задание 3");
            i = -10;
            j = 0;
            while (i <= 12 && j <= 4)
            {
                Console.Write(i + " ");
                i = i + 1;
                if (i == 13)
                {
                    j = j + 1;
                    i = -10;
                    Console.WriteLine();
                }
            }

            //Num 4
            Console.WriteLine();
            Console.WriteLine();
            Console.WriteLine("Задание 4");
            i = 41;
            j = 0;
            while (i <= 80)
            {
                Console.Write((i) + " ");
                i = i + 1;
                j = j + 1;
                if (j % 10 == 0)
                {
                    Console.WriteLine();
                    Console.WriteLine();
                }

            }

            //Num 5
            Console.WriteLine();
            Console.WriteLine();
            Console.WriteLine("Задание 5");
            for (i = 1; i <= 5; i++, Console.WriteLine())
            {
                for (j = 1; j <= i; j++)
                {
                    Console.Write(5 + " ");
                }
            }

            //Num 6
            Console.WriteLine();
            Console.WriteLine();
            Console.WriteLine("Задание 6");
            for (i = 5; i >= 1; i--, Console.WriteLine())
            {
                for (j = 1; j <= i; j++)
                {
                    Console.Write(1 + " ");
                }
            }

            //Num 7
            Console.WriteLine();
            Console.WriteLine();
            Console.WriteLine("Задание 7");
            for (i = 1; i <= 5; i++, Console.WriteLine())
            {
                for (j = 1; j <= i; j++)
                {
                    Console.Write(i + " ");
                }
            }

            //Num 8 
            Console.WriteLine();
            Console.WriteLine();
            Console.WriteLine("Задание 8");
            for ( i = 1; i <= 5; i++)
            {
                Console.WriteLine();
                for (j = 5; j >= i; j--)
                {
                    Console.Write(5 + i);
                }
            }




            //Num 9
            Console.WriteLine();
            Console.WriteLine();
            Console.WriteLine("Задание 9");
            for (i = 1; i <= 5; i++)
            {
                Console.WriteLine();
                for (j = 1; j <= i; j++)
                {
                    Console.Write(8 - i);
                }
            }


            //Num 10 
            Console.WriteLine();
            Console.WriteLine();
            Console.WriteLine("Задание 10");
            for (i = 8; i >= 4; i--, Console.WriteLine())
            {
                for (j = 4; j <= i; j++)
                {
                    Console.Write(i + " ");
                }
            }

            //Num 11
            Console.WriteLine();
            Console.WriteLine();
            Console.WriteLine("Задание 11");
            for (i = 1; i <= 5; i++, Console.WriteLine())
            {
                for (j = 1; j <= i; j++)
                {
                    Console.Write(j + " ");
                }
            }

            //Num 12
            Console.WriteLine();
            Console.WriteLine();
            Console.WriteLine("Задание 12");
            for (i = 1; i <= 5; i++)
            {
                Console.WriteLine();
                for (j = i; j >= 1; j--)
                {
                    Console.Write(j + " ");
                }
            }

            //Num 13
            Console.WriteLine();
            Console.WriteLine();
            Console.WriteLine("Задание 13");
            for (i = 4; i >= 0; i--)
            {
                Console.WriteLine();
                for (j = 0; j <= i; j++)
                {
                    Console.Write(j + " ");
                }
            }

            //Num 14
            Console.WriteLine();
            Console.WriteLine();
            Console.WriteLine("Задание 14");
            for (i = 0; i <= 4; i++)
            {
                Console.WriteLine();
                for (j = 4; j >= i; j--)
                {
                    Console.Write(j + " ");
                }
            }

            //Num 15
            Console.WriteLine();
            Console.WriteLine();
            Console.WriteLine("Задание 15");
            for (i = 1; i <= 5; i++)
            {
                Console.WriteLine();
                for (j = 1; j <= i; j++)
                {
                    Console.Write(i + " ");
                }
                Console.WriteLine();
                for (int k = 1; k <= i; k++)
                {
                    Console.Write(0 + " ");
                }
            }
            Console.ReadKey();
            //Num 16





































































        }
    }
}
