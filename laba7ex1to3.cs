﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace laba7
{


    class Program
    {
        static void First(int[,] quack)
        {
            Console.Write("Введите номер строки, которую необходимо вывыести: ");
            int k = int.Parse(Console.ReadLine());
            k = k - 1;
            for (int i = k; i == k; i++)
            {
                for (int j = 0; j < 4; j++)
                {
                    Console.Write("{0}", quack[i, j]);
                }
            }
        }

        static void Second(int[,] quack)
        {
            Console.Write("Введите номер столбца, который необходимо вывести: ");
            int k = int.Parse(Console.ReadLine());
            k = k - 1;
            for (int i = 0; i < 3; i++)
            {
                for (int j = k; j == k; j++)
                {
                    Console.Write("{0}", quack[i, j]);
                    Console.WriteLine();
                }
            }
        }

        static void Sum(int[,] quack)
        {
            int sum = 0;
            for (int i = 0; i < quack.GetLength(0); i++)
                for (int j = 0; j < quack.GetLength(1); j++)
                {
                    if (i == 2)
                    {
                        sum += quack[i, j];
                    }
                }
            Console.Write("Суммма элементов 3-ей строки равна: ");
            Console.WriteLine("{0}", sum);
        }


        

        static void Main(string[] args)
        {
            Console.WriteLine("Задание 1.");
            Console.WriteLine();
            int[,] quack = new int[3, 4];
            Console.WriteLine("Введите массив: ");

            for (int i = 0; i < quack.GetLength(0); i++)
            {
                for (int j = 0; j < quack.GetLength(1); j++)
                {
                    Console.Write("Значение для {0} - ой строки, {1} - го столбца: ", i + 1, j + 1);
                    quack[i, j] = int.Parse(Console.ReadLine());
                    Console.WriteLine();
                }
            }
            Console.WriteLine();
            Console.WriteLine("Полный массив: ");
            for (int i = 0; i < quack.GetLength(0); i++)
            {
                for (int j = 0; j < quack.GetLength(1); j++)
                {
                    Console.Write(quack[i, j] + " ");
                    if (j == 4)
                        Console.WriteLine();
                }
                Console.WriteLine();
            }
            First(quack);
            Console.WriteLine();
            Second(quack);
            Console.WriteLine();
            Sum(quack);
            Console.WriteLine();
            Console.WriteLine();


            Console.WriteLine("Задание 2.");
            Console.WriteLine();
            Console.WriteLine("Полный массив: ");
            for (int i = 0; i < quack.GetLength(0); i++)
            {
                for (int j = 0; j < quack.GetLength(1); j++)
                {
                    Console.Write(quack[i, j] + " ");

                    if (j == 3)
                        Console.WriteLine();
                }
            }
            Console.WriteLine();
            Console.WriteLine("Изменённый массив: ");
            int temp = 0;
            for (int i = 0; i < quack.GetLength(0)/2; i++)
            {
                for (int j = 0; j < quack.GetLength(1); j++)
                {
                    temp = quack[i, j];
                    quack[i, j] = quack[quack.GetLength(0) - i - 1, j];
                    quack[quack.GetLength(1) - i-2, j] = temp; 
                }
            }
            for (int i = 0; i < quack.GetLength(0); i++)
            {
                for (int j = 0; j < quack.GetLength(1); j++)
                {
                    Console.Write(quack[i, j] + " ");
                    if (j == 3)
                    {
                        Console.WriteLine();
                    }
                }
            }
            Console.WriteLine();
            Console.WriteLine();


            Console.WriteLine("Задание 3.");
            int[,] vagoon = new int[18, 36];
            Random hello = new Random();
            for (int i = 0; i < vagoon.GetLength(0); i++) 
            {
                for (int j = 0; j < vagoon.GetLength(1); j++) 
                {
                    vagoon[i,j] = hello.Next(0, 2);
                }
            }
            for (int i = 1; i < vagoon.GetLength(0); i++)
            {
                for (int j = 1; j < vagoon.GetLength(1); j++)
                {
                    if (vagoon[i, j] == 0)
                        Console.WriteLine("Место под номером {0}, в вагоне под номером {1} явяется свободным для покупки.",j,i);
                    else
                        Console.WriteLine("Место под номером {0}, в вагоне под номером {1} явяется купленным.",j,i);
                    Console.WriteLine();
                }
            }
                Console.ReadKey();
            }
        }
    }

