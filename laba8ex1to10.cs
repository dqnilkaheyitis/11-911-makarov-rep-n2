﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace laba8
{
    class Program
    {
        //56
        static void Off(string s, ref string count1, out int w1)
        {
            char x = ' ';
            char y = ' ';
            w1 = s.Length;
            for (int i = 0; i < s.Length-2; i = i + 2)
            {
                x = s[i];
                y = s[i + 1];
                count1 = count1 + y + x;
            }
            if (s.Length%2!=0)
            {
                count1 += s[s.Length-1];
            }
        }

        static void Count(string s, out int x)
        {
            x = 0;
            for (int i = 0; i < s.Length; i++)
            {
                if (char.IsLetter(s[i]))
                    x++;
            }
        }

        static int Mirror(string s, int w1)
        {
            w1 = 0;
            for (int i = 0; i < s.Length - 1; i++)
            {
                if (s[i] == s[i + 1])
                {
                    w1 = 1;
                }
            }
            return w1;
        }

        static string Del(string s)
        {
            int x1 = s.Length;
            if (x1 % 2 != 0)
                for (int i = (s.Length - 1) / 2 + 1; i == (s.Length - 1) / 2 + 1; i++) 
                {
                    int q = i;
                    s = s.Remove(q-1, 1);
                }
            else
                for (int i = s.Length / 2; i == s.Length / 2; i++) 
                {
                    int q = i;
                    s = s.Remove(q-1, 2);
                }
            return s;
        }

        static int CCount(string s, int count_1)
        {
            
            for (int i = 0; i < s.Length; i++)
            {
                if (char.IsDigit(s[i]))
                    count_1++;
            }
            return count_1;
        }

        static string Go(string s)
        {
            for (int i = 0; i < s.Length; i++)
            {
                char s1 = s[i];
                if (s1 == ':')
                {
                    s = s.Remove(i,s.Length-i);
                }
            }
            return s;
        }

        static string Bo(string s)
        {
            for (int i = s.Length-1; i > 0; i--) 
            {
                char s1 = s[i];
                if (s1 == ':')
                    s = s.Remove(0, s.Length-i-3);
            }
            return s;
        }

        static string Do(string s)
        {
            int s2 = 0;
            int s3 = 0;
            int s4 = 0;
            for (int i = 0; i < s.Length; i++)
            {
                char s1 = s[i];
                if (s1 == ',')
                {
                    if (s3 == 0)
                    {
                        s2 = i;
                        s3++;
                    }
                    else
                    {
                        s4 = i;
                    }
                }
            }
            s = s.Remove(s2+1, s4 - s2);
            return s;
        }

        static int howmuch(string s)
        {
            int c1 = 0;
            int c2 = 0;
            int c3 = 0;
            int c4 = 0;
            int c5 = 0;
            int c6 = 0;
            int c7 = 0;
            int c8 = 0;
            for (int i = 0; i < s.Length; i++)
            {
                char s1 = s[i];
                if (char.IsControl(s[i]) & c1 == 0)
                    c1++;
                if (char.IsDigit(s[i]) & c2 == 0)
                    c2++;
                if (char.IsLetter(s[i]) & c3 == 0)
                    c3++;
                if (char.IsLower(s[i]) & c4 == 0)
                    c4++;
                if (char.IsPunctuation(s[i]) & c5 == 0)
                    c5++;
                if (char.IsSeparator(s[i]) & c6 == 0)
                    c6++;
                if (char.IsWhiteSpace(s[i]) & c7 == 0)
                    c7++;
            }
            c8 = c1 + c2 + c3 + c4 + c5 + c6 + c7;
            return c8;
        }

            static void Main(string[] args)
        {
            Console.WriteLine("Задание 1.");
            Console.Write("Введите слово: ");
            string s = Console.ReadLine();
            string s1 = s;
            int w1;
            string count1 = "";
            Off(s,ref count1, out w1);
            Console.WriteLine("Новое слово - {0}.", count1);

            Console.WriteLine();
            Console.WriteLine("Задание 2.");
            Console.Write("Введите слово: ");
            int x;
            Count(s, out x);
            Console.WriteLine("Количество букв в слове {0} - {1}", s, x);
            Console.WriteLine();


            Console.WriteLine("Задание 3.");
            Mirror(s, w1);
            if (Mirror(s,w1) == 1)
                Console.WriteLine("В строке имеются две соседствующие однинаковые буквы.");
            else
                Console.WriteLine("В строке HE имеются две соседствующие однинаковые буквы.");
            Console.WriteLine();

            Console.WriteLine("Задание 4.");
            Del(s);
            Console.Write("Измененное слово: ");
            Console.WriteLine(Del(s));
            Console.WriteLine();

            Console.WriteLine("Задание 5.");
            Console.Write("Введите строку для замены: ");
            string substr1 = Console.ReadLine();
            Console.Write("Введите строку на замену: ");
            string substr2 = Console.ReadLine();
            string substr3 = substr2.Replace(substr1, substr2);
            Console.WriteLine(substr3);

            Console.WriteLine("Задание 6.");
            int count_1 = 0;
            CCount(s, count_1);
            Console.WriteLine("Количество цифр в слове {0} = {1}", s, CCount(s,count_1));
            Console.WriteLine();

            Console.WriteLine("Задание 7.");
            Go(s);
            Console.Write("Последоватледовальность символов перед первым двоеточием: ");
            Console.WriteLine(Go(s));
            Console.WriteLine();

            Console.WriteLine("Задание 8.");
            Bo(s);
            Console.Write("Последовательность символов после последнего двоеточия: ");
            Console.WriteLine(Bo(s));
            Console.WriteLine();

            Console.WriteLine("Задание 9.");
            Do(s);
            Console.Write("Строка без символов между двумя запятыми: ");
            Console.WriteLine(Do(s));
            Console.WriteLine();

            Console.WriteLine("Задание 10.");
            Console.Write("Введите слово на символьную проверку: ");
            s = Console.ReadLine();
            howmuch(s);
            Console.Write("Различных символов в строке: ");
            Console.WriteLine(howmuch(s));
            Console.ReadKey();
        }
    }
}
