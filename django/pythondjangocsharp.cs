﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Django
{
    class FIO
    {
      public string Name { get; set; }

      public string LastName { get; set; }

      public int Year { get; set; }

      public string Group { get; set; }



        public override string ToString()
        {
            return $" {Name} {LastName} {Year} {Group}";
        }

        public void Print()
        {
            Console.WriteLine($"Name: {Name} \nLastName: {LastName} \nYear: {Year} \nGroup: {Group}");
        }

    }


    class Program
    {
        static void Main(string[] args)
        {
            FIO f = new FIO() { Name = "Danila", LastName = "Makarov", Year = 2001, Group = "11-911" };

            f.Print();
        }
    }
}
