﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace oop3
{
    class Line : Function
    {
        public double a;
        public double b;
        public double x;
        public Line()
        {

        }


        public Line(double a, double b, double x)
        {
            this.a = a;
            this.b = b;
            this.x = x;
        }

        public override double Func()
        {
            return a * x + b;
        }

        public override string NameFunc()
        {
            string s = "Функция ax + b: " + Func();
            return s;
        }



    }
}
