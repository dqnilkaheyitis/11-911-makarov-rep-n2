﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Text.RegularExpressions;

namespace controlwork2
{
    class Example
    {
        static void num(string s, int x)
        {
            int d = 9;
            int v = 0;

            Random random = new Random();
            Regex regex = new Regex(@"[2|4|6|8]\s[2|4|6|8]");
            while (s== " " || s == "")
            {
                for (int i = 0; i < d; i++)
                {
                    x = random.Next(0, 10);
                    s = s + Convert.ToString(x) + " ";
                    v++;
                    if (v == 9)
                    {
                        MatchCollection matches = regex.Matches(s);
                        if (matches.Count > 0)
                        {
                            Console.Write("Полученная строка: ");
                            Console.WriteLine(s);
                        }
                        else
                            i = 0;
                    }
                }

            }
        }

        static void table(int [,] w,int n, int m )
        {
            int c = 0;
            Random random = new Random();
            Console.WriteLine("В заданной таблице счёт определенной команды указана на определенной строке, например, для команды про название ' 0 ', счёт указан на 0-ой строке. ");
            for (int i = 0; i < w.GetLength(0); i++)
                for (int j = 0; j < w.GetLength(1); j++)
                {
                    if (j == c)
                    {
                        w[i, j] = 0;
                    }
                    else
                    {
                        w[i, j] = random.Next(0, 4);
                        if (w[i, j] == 2)
                            w[i, j] = 3;
                    }
                    if (j == m-1)
                    {
                        c++;
                    }
                   
                }
            Console.WriteLine();
            Console.WriteLine("ТАБЛИЦА ОЧКОВ: ");
            for (int i = 0; i < w.GetLength(0); i++)
                for (int j = 0; j < w.GetLength(1); j++)
                {
                    if (j == 0)
                    {
                        Console.Write("{0}-я команда: ", i);
                    }
                    Console.Write(w[i, j] + " ");
                    if (j == m - 1)
                        Console.WriteLine();
                }
            int win = 0;
            int lose = 0;
            string f = " ";
            for (int i = 0; i < w.GetLength(0); i++)
                for (int j = 0; j < w.GetLength(1); j++)
                {
                    if (w[i, j] == 3)
                        win++;
                    else if (w[i, j] == 0)
                    {
                        lose++;
                    }
                    if (j == m - 1)
                    {
                        if (win > lose)
                            f = f + Convert.ToString(i) + " - команда ";
                        win = 0;
                        lose = 0;
                    }
                }
            if (f != " ")
                Console.WriteLine("Список команд имеющих больше побед, чем поражений: " + f);
            else
                Console.WriteLine("Команд, имеющих больше побед, чем поражений нет.");
            int not_lose = 0;
            string h = " ";
            for (int i = 0; i < w.GetLength(0); i++)
                for (int j = 0; j < w.GetLength(1); j++)
                {
                    if(w[i,j]!=0)
                    {
                        not_lose++;
                    }
                    if(j==m-1)
                    {
                        if (not_lose == 4)
                        {
                            h = h + Convert.ToString(i) + "- команда ";
                            not_lose = 0;
                        }
                    }
                }
            if (h != " ")
                Console.Write("Команды, которые прошли чемпионат без единого поражения: " + h);
            else
                Console.WriteLine("Команд, прошедших турнир без поражений нет.");
        }
        static void Main(string[] args)
        {
            Console.WriteLine("Задание 1.");
            string s = " ";
            int x = 0;
            num(s, x);
            Console.WriteLine();

            Console.WriteLine("Задание 2.");
            Console.Write("Введите количество строк: ");
            int n = int.Parse(Console.ReadLine());
            Console.WriteLine();
            Console.Write("Введите количество столбцов: ");
            int m = int.Parse(Console.ReadLine());
            Console.WriteLine();
            int[,] w = new int[n,m];
            table(w,n,m);


            Console.ReadKey();
        }
    }
}
