﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace laba5
{
    class Program
    {
        static void input_x(out string x, out int x_count, out int x_cost, out int x_sale)
        {
            int x_count1;
            Console.Write("Введите наименование для первого товара: ");
            x = (Console.ReadLine());
            Console.Write("Введите количество для товара - {0}: ",x);
            x_count = int.Parse(Console.ReadLine());
            Console.Write("Введите цену для товара - {0}: ",x);
            x_cost = int.Parse(Console.ReadLine());
            Console.Write("Введите скидку для товара - {0}: ",x);
            x_sale = int.Parse(Console.ReadLine());
            Console.WriteLine("Товар - {0}, Скидка - {1}, Количество - {2}, Цена - {3}", x, x_sale, x_count, x_cost);
            Console.Write("Расчёт стоимости товара {0} с учётом скидки: ", x);
            x_count1 = ((x_cost) - (((x_cost) / 100) * x_sale));
            Console.WriteLine(x_count1 + " " + "рублей.");
            Console.Write("Расчёт стоимости товара {0} с учётом скидки и количества: ", x);
            x_count = ((x_count*x_cost)-(((x_count*x_cost)/100)*x_sale));
            Console.WriteLine(x_count + " " + "рублей.");
            x_sale = x_count1;

        }
        static void input_y(out string y, out int y_count, out int y_cost, out int y_sale)
        {
            int y_count1;
            Console.Write("Введите наименование для второго товара: ");
            y = (Console.ReadLine());
            Console.Write("Введите количество для товара - {0}: ", y);
            y_count = int.Parse(Console.ReadLine());
            Console.Write("Введите цену для товара - {0}: ", y);
            y_cost = int.Parse(Console.ReadLine());
            Console.Write("Введите скидку для товара - {0}: ", y);
            y_sale = int.Parse(Console.ReadLine());
            Console.WriteLine("Товар - {0}, Скидка - {1}, Количество - {2}, Цена - {3}", y, y_sale, y_count, y_cost);
            Console.Write("Расчёт стоимости товара {0} с учётом скидки: ", y);
            y_count1 = ((y_cost) - (((y_cost) / 100) * y_sale));
            Console.WriteLine(y_count1 + " " + "рублей.");
            Console.Write("Расчёт стоимости товара {0} с учётом количества и скидки: ", y);
            y_count = ((y_count * y_cost) - (((y_count * y_cost) / 100) * y_sale));
            Console.WriteLine(y_count + " " + "рублей.");
            y_sale = y_count1;
        }

        static void min_cost(int y_sale, int x_sale, int Boeing_777_cost)
        {
            if (y_sale < x_sale && y_sale < Boeing_777_cost)
                Console.WriteLine("Наименьшая стоимость у товара под название Самолёт.");
            else if (x_sale < y_sale && x_sale < Boeing_777_cost) 
                Console.WriteLine("Наименьшая стоимость у товара под именем Машина.");
            else if (Boeing_777_cost < x_sale && Boeing_777_cost < y_sale) 
            Console.WriteLine("Наименьшая стоимость у товара под именем Boeing_777.");
                    
        }
        static void all_cost(int y_count, int x_count, int Boeing_777_count)
        {
            int all_c;
            Console.Write("Стоимость всех товаров: ");
            all_c = y_count + x_count + Boeing_777_count;
            Console.WriteLine(all_c + " " + "рублей.");
        }

        static int middlear(int x1, int y1)
        {
            return ((x1 + y1) / 2);
        }

        static int middlear(int x1, int y1, int z)
        {
            return ((x1 + y1 + z) / 3);
        }
        
        static int middlear(int x1, int y1, int z, int w)
        {
            return (x1 + y1 + z + w) / 4;
        }

        static double middlear(double x1)
        {
            double S = 0;
            Console.Write("Введите необходимое количество переменных, которое вам необходимо для нахождения среднее-арифметического значения: ");
            double z1 = double.Parse(Console.ReadLine());
            int a9 = 1;
            for (int u = 1; u <= z1; u++)
            {
                Console.WriteLine("{0}-ая переменная: ", a9);
                x1 = double.Parse(Console.ReadLine());
                S += x1;
                a9++;
            }
            return (S/z1);
        }

        static void bilet(int b1)
        {
            int b1_1 = 0;
            int b1_2_1 = 0;
            int b1_11 = b1;
            while (b1_11 > 0) 
            {
                if (b1_11 > 999)
                {
                    b1_1 += b1_11 % 10;
                    b1_11 = b1_11 / 10;
                }
                if (b1_11<=999)
                {
                    b1_2_1 += b1_11 % 10;
                    b1_11 = b1_11 / 10;
                }

            }
            if (b1_2_1 == b1_1) 
                Console.WriteLine("Билет счастливый!");
            else
                Console.WriteLine("Билет не счастливый..");
        }

        static void bilet(int b1, int b2, int b3, int b4, int b5, int b6)
        {
            int b1_2;
            int b1_3;
            b1_2 = b1 + b2 + b3;
            b1_3 = b4 + b5 + b6;
            if (b1_2 == b1_3)
                Console.WriteLine("Билет счастливый!");
            else
                Console.WriteLine("Билет не счастливый..");
        }

        static void bilet(int b1_222, int b1_22)
        {
            int n = 0;
            int m = 0;
            while (b1_222 > 0 || b1_22 > 0)
            {
                if (b1_222 > 0)
                {
                    n = n + (b1_222 % 10);
                    b1_222 = b1_222 / 10;
                }
                if (b1_22 > 0)
                {
                    m = m + (b1_22 % 10);
                    b1_22 = b1_22 / 10;
                }
            }
            if (b1_22 == b1_222)
                Console.WriteLine("Билет счастливый!");
            else
                Console.WriteLine("Билет не счастливый..");
        }

    

        static void Main(string[] args)
        {
            //Num 1.
            Console.WriteLine("Задание 1.");
            Console.WriteLine();
            string x; int x_count; int x_cost; int x_sale;
            string y; int y_count; int y_cost; int y_sale;
            string Boeing_777 = "Boeing_777"; int Boeing_777_count = 2; int Boeing_777_cost = 1000000; string Boeing_777_sale = "скидка отсутсвует." ;
            Console.WriteLine("Товар - {0}, Скидка - {1}, Количество - {2}, Цена - {3}", Boeing_777, Boeing_777_sale, Boeing_777_count, Boeing_777_cost);
            Console.Write("Расчёт стоимости товара {0} с учётом скидки: ", Boeing_777);
            Console.WriteLine(Boeing_777_cost + " " + " рублей.");
            Console.Write("Расчёт стоимости товара {0} с учётом количества и скидки: ", Boeing_777);
            Boeing_777_count = ((Boeing_777_count * Boeing_777_cost));
            Console.WriteLine(Boeing_777_count + " " + " рублей.");
            Console.WriteLine();
            input_x(out x, out x_count, out x_cost, out x_sale);
            Console.WriteLine();
            input_y(out y, out y_count, out y_cost, out y_sale);
            Console.WriteLine();
            min_cost(y_sale, x_sale, Boeing_777_count);
            Console.WriteLine();
            all_cost(y_count, x_count, Boeing_777_count);
            Console.WriteLine();
            Console.WriteLine();




            //Num 2.
            Console.WriteLine("Задание 2.");
            Console.WriteLine();
            int z; int w; int x1; int y1;
            Console.WriteLine("Введите значение для первой переменной.");
            x1 = int.Parse(Console.ReadLine());
            Console.WriteLine("Введите значение для второй переменной.");
            y1 = int.Parse(Console.ReadLine());
            Console.WriteLine("Введите значение для третьей переменной.");
            z = int.Parse(Console.ReadLine());
            Console.WriteLine("Введите значение для четвёртой переменной.");
            w = int.Parse(Console.ReadLine());
            Console.WriteLine("2.1");
            middlear(x1, y1);
            Console.WriteLine("Среднее Арифметическое по 2-м переменным равно: {0}", middlear(x1, y1));
            Console.WriteLine("2.2");
            middlear(x1, y1, z);
            Console.WriteLine("Среднее Арифметическое по 3-м переменным равно: {0}", middlear(x1, y1, z));
            Console.WriteLine("2.3");
            middlear(x1, y1, z, w);
            Console.WriteLine("Среднее Арифметическое по 4-м переменным равно: {0}", middlear(x1, y1, z, w));
            Console.WriteLine("2.4");
            double i = middlear(x1);
            Console.WriteLine("Среднее Арифметическое через метод ввода производного количества переменных: {0}", i);
            Console.WriteLine();
            Console.WriteLine();




            //Num 3.
            Console.WriteLine("Задание 3.");
            Console.WriteLine();
            Console.Write("Введите 6-ти значное число: ");
            int b1 = int.Parse(Console.ReadLine());
            if (b1 > 999999 || b1 < 100000)
            {
                while (b1 > 999999 || b1 < 100000)
                {
                    Console.WriteLine("Число Б1 не является 6-ти значным, пожалуйста, введите его заного.");
                    b1 = int.Parse(Console.ReadLine());
                }
            }
            else
                Console.WriteLine(); Console.WriteLine("Ура, вы ввели то, что вам сказали!");
            Console.WriteLine();
            int b1_22 = b1;
            Console.WriteLine("По первому заданию.");
            bilet(b1);
            Console.WriteLine();
            int b2; int b3; int b4; int b5; int b6;
            b6 = b1 % 10;
            b1 = b1 / 10;
            b5 = b1 % 10;
            b1 = b1 / 10;
            b4 = b1 % 10;
            b1 = b1 / 10;
            b3 = b1 % 10;
            b1 = b1 / 10;
            b2 = b1 % 10;
            b1 = b1 / 10;
            Console.WriteLine("По второму заданию.");
            bilet(b1, b2, b3, b4, b5, b6);
            Console.WriteLine();
            int b1_222;
            b1_222 = b1_22 % 1000;
            b1_22 = b1_22 / 1000;
            Console.WriteLine("По третьему заданию.");
            bilet(b1_222, b1_22);
            Console.ReadKey();
        }
    }
}
