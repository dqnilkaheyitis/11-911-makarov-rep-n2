﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ControlWorkVar1
{
    class AEROFL0T 
    {
        public string nameofbase { get; set; }
        public string numberofrace { get; set; }
        public string typeofplane { get; set; }



        public AEROFL0T()
        {

        }


        public AEROFL0T(string nameofbase, string numberofrace, string typeofplane)
        {
            this.nameofbase = nameofbase;
            this.numberofrace = numberofrace;
            this.typeofplane = typeofplane;
        }


         int CompareTo(AEROFL0T obj)
        {
            if ((numberofrace.CompareTo(obj.numberofrace)) > 0)
                return 1;
            else
                return 0;
        }

    }
}
