﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace laba16
{
    class SimpleLongNumber : INumber
    {
        public long q;
        public long q1 { get; set; }

        public SimpleLongNumber()
        {

        }

        public SimpleLongNumber(long g)
        {
            this.q1 = g;
        }


        public INumber add(INumber n)
        {
            SimpleLongNumber a = (SimpleLongNumber)n;
            q1 += a.q1;
            return this;

        }

        public INumber sub(INumber n)
        {
            SimpleLongNumber a = (SimpleLongNumber)n;
            if (q1 < a.q1)
            {
                throw new Exception("NotNaturalNumberException");
            }
            else
            {
                q1 -= a.q1;
                return this;
            }
        }

        public int CompareTo(INumber n)
        {
            SimpleLongNumber a = (SimpleLongNumber)n;
            int x = q1.CompareTo(a.q1);
            return x;
        }

    }
}
