﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace oop3
{
    class Rectangle : Figure
    {
        public double x;
        public double y;

        public Rectangle()
        {

        }

        public Rectangle(double x, double y)
        {
            this.x = x;
            this.y = y;
        }

        public override double S()
        {
            return x  * y;
        }

        public override double P()
        {
            return 2 * (x + y);
        }

        public override string FigureInfo()
        {
            string s = "S: " + Convert.ToString(S() + " P: " + Convert.ToString(P()));
            return s;
        }
        public override string Perm()
        {
            string s = "Стороны прямоугольника: " + " " + Convert.ToString(x) + " " + Convert.ToString(y);
            return s;
        }
    }
}
