﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace laba16
{
    class VeryLongNumber : INumber
    {
        public long q1 { get; set; }


        public VeryLongNumber()
        {

        }

        public VeryLongNumber(string q)
        {
            this.q1 = Convert.ToInt32(q);
        }

        public VeryLongNumber(int q)
        {
            this.q1 = q;
        }

        public INumber add(INumber n)
        {
            VeryLongNumber d = (VeryLongNumber)n;
            long n_1 = n.q1; 
            q1 += n_1;
            return this;
        }
        public INumber sub(INumber n)
        {
            VeryLongNumber d = (VeryLongNumber)n;
            if (q1 < d.q1)
            {
                throw new Exception("NotNaturalNumberException");
            }
            else
            {
                q1 -= d.q1;
                return this;
            }
        }

        public int CompareTo(INumber n)
        {
            VeryLongNumber d = (VeryLongNumber)n;
            int x = q.CompareTo(d.q);
            return x;
        }
    }
}
