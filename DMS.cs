﻿using DocumentFormat.OpenXml.Wordprocessing;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CsvHelper;
using System.Collections;

namespace DataMiningFirstEx
{

    public class Transaction
    {
        public string PROD_CODE { get; set; }

        public string BASKET_ID { get; set; }
        public void piece(string line)
        {
            string[] parts = line.Split(';');  
            PROD_CODE = parts[0];
            BASKET_ID = parts[1];
        }

        public override string ToString()
        {
            return $"PROD_CODE: {PROD_CODE}, BASKET_ID: {BASKET_ID}";
        }

        public static List<Transaction> ReadFile()
        {
            List<Transaction> list = new List<Transaction>();

            string path = @"C:\Users\Danila\Downloads\Telegram Desktop\transactions.csv";

            using (StreamReader sr = new StreamReader(path))
            {
                    string line;
                    while ((line = sr.ReadLine()) != null)
                    {
                        {
                            Transaction tr = new Transaction();
                            tr.piece(line);
                            list.Add(tr);
                        }
                    }
                    
            }

            return list;
        }

    }
    
    class Program
    {
        static void Main(string[] args)
        {


            var bob = Transaction.ReadFile();


            var bab = bob.GroupBy(g => g.PROD_CODE).Select(count =>
            new
            {
                PROD_CODE = count.Key,
                COUNT = count.Count()
            }).OrderBy(o => o.COUNT).Distinct();


            var bib = bab.Select(s =>
            new
            {
                s.PROD_CODE,
                s.COUNT
            });

            
            using (StreamWriter sw = new StreamWriter(@"C:\Users\Danila\Desktop\yey.txt"))
            {

                foreach (var i in bib)
                {
                    sw.WriteLine(i.PROD_CODE + "," + i.COUNT);
                }
            }




        }
    }
}
