﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace oop3
{
    class Triangle : Figure
    {
        public double x;
        public double y;
        public double z;

        public Triangle()
        {

        }

        public Triangle(double x, double y, double z)
        {
            this.x = x;
            this.y = y;
            this.z = z;
        }

        public override double S()
        {
            return (x * y)/2;
        }

        public override double P()
        {
            if ((x + y) > z)
            {
                return (x + y + z);
            }
            else
            {
                Console.WriteLine("Такового треугольника не существует, введите данные заного: ");
                Environment.Exit(0);
                return 0;
            }
        }

        public override string FigureInfo()
        {
            string s = "S: " + Convert.ToString(S() + " P: " + Convert.ToString(P()));
            return s;
        }

        public override string Perm()
        {
            string s = "Стороны треугольника: " + " " + Convert.ToString(x) + " " + Convert.ToString(y) + " " + Convert.ToString(z);
            return s;
        }
    
    }
}
