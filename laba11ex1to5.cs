﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Text.RegularExpressions;

namespace laba11
{
    class Example
    {
        static void first(string s)
        {
            Regex regex = new Regex(@"\d{3}-\d{3}|\d{2}-\d{2}-\d{2}|\d{3}-\d{2}-\d{2}");
            MatchCollection mathes = regex.Matches(s);
            if (mathes.Count > 0)
                foreach (Match bob in mathes)
                    Console.WriteLine(bob.Value);
            else
                Console.WriteLine("Таких номеров нет в наличии.");
        }
        static void sec(string s)
        {
            Regex regex = new Regex(@"(([0-2][0-9])|(3[0-1]))\.((0[1-9])|(1[0-2]))\.((200[0-9])|(19[0-9][0-9]))");
            MatchCollection mathes = regex.Matches(s);
            if (mathes.Count > 0)
                foreach (Match bob in mathes)
                    Console.WriteLine("Дата из диапазона от 1900 до 2010 года: " + bob.Value);
            else
                Console.WriteLine("Заданных дат нет.");
        }

        static void the(string s)
        {
            Regex regex = new Regex(@"(25[0-5]|2[0-4][0-9]|[0-1]?[0-9][0-9]?)(\.(25[0-5]|2[0-4][0-9]|[0-1]?[0-9][0-9]?)){3}");
            MatchCollection matches = regex.Matches(s);
            if (matches.Count > 0)
            {
                foreach (Match bob in matches)
                    Console.WriteLine("Действительный IP - адресс: " + bob.Value);
            }
            else
                Console.WriteLine("Заданных ip-адрессов не существует.");
            Console.Write("Введите число, если последняя триада ip-адресса будет начинается с этой цифрой, то такой ip-адресс будет удалён: ");
            int x = int.Parse(Console.ReadLine());
            Regex regex1 = new Regex(@"(25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)(\.(25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?))(25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)(\.(25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?))(\.(" + x.ToString() + @"[0-5]|" + x.ToString() + @"[0-4][0-9]|[01]?[" + x.ToString() + @"][0-9]?))");
            string t = regex1.Replace(s, string.Empty);
            MatchCollection matches1 = regex.Matches(t);
            if (matches1.Count > 0)
            {
                foreach (Match bob1 in matches1)
                    Console.WriteLine("Остался: " + bob1.Value);
            }
            else
                Console.WriteLine("Все адресса были удаленны.");
        }
        static void four(string s)
        {
            Regex regex = new Regex(@"((www\.)*|(https:\\)*|(http:\\)*)\w+(\.org|\.net|\.com|\.ru|\.onion)");
            MatchCollection baba1 = regex.Matches(s);
            if (baba1.Count > 0)
            {
                foreach (Match bib in baba1)
                    Console.WriteLine("Действительный web-сайт: " + bib.Value);
            }
            else
                Console.WriteLine("Нет дейстаительных web-сайтов.");

        }

        static void five(string s)
        {
            Regex regex = new Regex(@"(([0-2][0-9])|(3[0-1]))\.((0[1-9])|(1[0-2]))\.((200[0-9])|(19[0-9][0-9]))");
            MatchCollection mathes = regex.Matches(s);
            if (mathes.Count > 0)
                foreach (Match bob in mathes)
                {
                    Console.WriteLine("Дата из диапазона от 1900 до 2010 года: " + bob.Value);
                    string[] w = (bob.Value).Split('.');
                    for (int i = 0; i < 1; i++)
                    {
                        int x = Convert.ToInt32(w[i]);
                        x++;
                        if (x < 10)
                        {
                            w[i] = "0";
                            w[i] = w[i] + Convert.ToString(x);
                        }
                        else
                            w[i] = Convert.ToString(x);
                        Console.WriteLine("Измененная дата: {0}.{1}.{2}",w[i],w[i+1],w[i+2]);
                        Console.WriteLine();
                    }
                }
           
        }
        static void Main(string[] args)
        {
            string s;

            Console.WriteLine("Задание 1.");
            Console.Write("Введите строку: ");
            s = Console.ReadLine();
            first(s);
            Console.WriteLine();

            Console.WriteLine("Задание 2.");
            Console.Write("Введите даты: ");
            s = Console.ReadLine();
            sec(s);
            Console.WriteLine();

            Console.WriteLine("Задание 3.");
            Console.Write("Введите IP-адресс: ");
            s = Console.ReadLine();
            the(s);
            Console.WriteLine();

            Console.WriteLine("Задание 4.");
            Console.Write("Введите web-сайты: ");
            s = Console.ReadLine();
            four(s);
            Console.WriteLine();

            Console.WriteLine("Задание 5.");
            Console.Write("Введите даты: ");
            s = Console.ReadLine();
            five(s);

            Console.ReadKey();
        }
    }
}