﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace lesson4
{
    class Program
    {
        //num1
        static int min(int a, int b)
        {
            if (a < b)
                return a;
            else
                return b;
        }

        static int max(int a, int b)
        {
            if (a > b)
                return a;
            else
                return b;
        }
        static void func(ref int x, ref int y)
        {
            int f;
            int f1;
            x = f = Convert.ToInt32(Math.Pow(x, 3) - Math.Sin(x));
            y = f1 = Convert.ToInt32(Math.Pow(y, 3) - Math.Sin(y)); 
        }

        static void comeback(ref int x)
        {
            x = Convert.ToInt32((x / 10) % 10);
        }

        static void kor(ref double x)
        {
            x = Convert.ToDouble(Math.Sqrt(x) + x);
        }

        static void last(ref int x)
        {
            if (x % 5 == 0)
                x = x / 5;
            else
                x++;
        }

 
        static void first(ref int x)
        {
            if (x % 2 != 0)
                x = 0;
            else
                x = x / 2;
        }

        static void block1(ref double x1)
        {
            if (x1 >= 0.9)
                x1 = (1 / Math.Pow((0.1 + x1), 2));
            else if (x1 < 0.9 || x1 >= 0)
                x1 = x1 * 0.2 + 0.1;
            else if (x1 < 0)
                x1 = Math.Pow(x1, 2) + 0.2;
        }
        static void block2(ref double x2)
        {
            if (Math.Abs(x2) < 3)
                x2 = Math.Sin(x2);
            else if (Math.Abs(x2) >= 3 || Math.Abs(x2) <= 9)
                x2 = (Math.Sqrt((Math.Pow(x2, 2) + 1))) / (Math.Sqrt((Math.Pow(x2, 2) + 5)));
            else if (Math.Abs(x2) > 0.2)
                x2 = (Math.Sqrt((Math.Pow(x2, 2) + 1))) - (Math.Sqrt((Math.Pow(x2, 2) + 5)));
        }

        static void block3(ref double x3, ref double a1)
        {
            if (x3 < a1)
                x3 = 0;
            else if (x3 > a1)
                x3 = (x3 - a1) / (x3 + a1);
            else if (x3 == a1)
                x3 = 1;
        }

        static void block4(ref double x4)
        {
            if (Math.Abs(x4) < 0.1)
                x4 = (Math.Pow(x4, 3) - 0.1);
            else if (Math.Abs(x4) <= 0.2 || Math.Abs(x4) > 0.1)
                x4 = 0.2 * x4 - 0.1;
            else if (Math.Abs(x4) > 0.2)
                x4 = (Math.Pow(x4, 3) + 0.1);
        }

        static void Main(string[] args)
        {

            //FIRST BLOCK

            //Num 1 

            Console.WriteLine("Задание 1.");
            Console.WriteLine();
            Console.Write("Введите x: ");
            int x =int.Parse(Console.ReadLine());
            Console.WriteLine();
            Console.Write("Введите y: ");
            int y = int.Parse(Console.ReadLine());
            Console.WriteLine();
            int z = min(3 * x, 2 * y) + min(x - y, x + y);
            Console.WriteLine("Значение выражения с учетом ипользования наименьших чисел равно: " + z );
            Console.WriteLine();
            Console.WriteLine();

            //Num 2

            Console.WriteLine("Задание 2.");
            Console.WriteLine();
            Console.Write("Введите a: ");
            int a = int.Parse(Console.ReadLine());
            Console.WriteLine();
            Console.Write("Введите b: ");
            int b = int.Parse(Console.ReadLine());
            var e1 = a;
            var e2 = b;
            Console.WriteLine();
            func(ref a, ref b);
            if (a > b)
                Console.WriteLine("Наибольшее значение принимает 'a'. При значении 'a' = {0}, функция принимает значение {1}.", e1, a);
            else
                Console.WriteLine("Наибольшее значение принимает 'b'. При значении 'b' = {0}, функция принимает значение {1}.", e2, b);
            Console.WriteLine();
            Console.WriteLine();

            //Num 3 

            Console.WriteLine("Задание 3.");
            Console.WriteLine();
            Console.WriteLine("Введите число > 9 для трёх переменных.");
            Console.WriteLine();
            Console.Write("Введите a: ");
            a = int.Parse(Console.ReadLine());
            Console.WriteLine();
            Console.Write("Введите b: ");
            b = int.Parse(Console.ReadLine());
            Console.WriteLine();
            Console.Write("Введите c: ");
            int c = int.Parse(Console.ReadLine());
            comeback(ref a);
            comeback(ref b);
            comeback(ref c);
            z = Convert.ToInt32(a + b - c);
            Console.WriteLine();
            Console.WriteLine("Ответ: " + z);
            Console.WriteLine();
            Console.WriteLine();

            //Num 4 

            Console.WriteLine("Задание 4.");
            double f = 6;
            double f1;
            double f2;
            double f3;
            kor(ref f);
            f1 = f;
            f = 13;
            kor(ref f);
            f2 = f;
            f = 21;
            kor(ref f);
            f3 = f;
            Console.WriteLine();
            Console.WriteLine("Значение выражения равно " + (f1/2 + f2/2 + f3/2));
            Console.WriteLine();
            Console.WriteLine();

            //Num 5

            Console.WriteLine("Задание 5.");
            Console.WriteLine();
            Console.Write("Введите значение для X: ");
            x = int.Parse(Console.ReadLine());
            Console.WriteLine();
            Console.WriteLine("Значение X до обработки - {0}", x);
            first(ref x);
            Console.WriteLine();
            Console.WriteLine("Значение X после обработки - {0}", x);
            Console.WriteLine();
            Console.WriteLine();

            //Num 6

            Console.WriteLine("Задание 6.");
            Console.WriteLine();
            Console.Write("Введите значение для X: ");
            x = int.Parse(Console.ReadLine());
            Console.WriteLine();
            Console.WriteLine("Значение X до обработки - {0}", x);
            last(ref x);
            Console.WriteLine();
            Console.WriteLine("Значение X после обработки - {0}", x);
            Console.WriteLine();
            Console.WriteLine();


            //SECOND BLOCK

            //Num 1
            Console.ForegroundColor = ConsoleColor.Red;
            Console.WriteLine("В ПОСЛЕДУЮЩИХ ЗАПРОСАХ ВВОДИТЕ ДАННЫЕ С ШАГОМ РАВНЫМ [0.1; 1]. ");

            Console.ForegroundColor = ConsoleColor.White;
            Console.WriteLine("Задание 2.1");
            Console.WriteLine();
            double y1;
            double x41;
            Console.Write("Введите значение для X: ");
            double x1 = double.Parse(Console.ReadLine());
            x41 = x1;
            Console.WriteLine();
            Console.WriteLine("Значение X до обработки - {0}", x1);
            block1(ref x1);
            y1 = x1;
            Console.WriteLine();
            Console.WriteLine("Значение функции Y(X) после обработки - {0}", y1);
            Console.WriteLine();
            Console.WriteLine();

            //Num 2

            Console.WriteLine("Задание 2.2");
            double y2;
            double x42;
            Console.WriteLine();
            Console.Write("Введите значение для X: ");
            double x2 = double.Parse(Console.ReadLine());
            x42 = x2;
            Console.WriteLine();
            Console.WriteLine("Значение X до обработки - {0}", x2);
            block2(ref x2);
            y2 = x2;
            Console.WriteLine();
            Console.WriteLine("Значение функции Y(X) после обработки - {0}", y2);
            Console.WriteLine();
            Console.WriteLine();

            //Num 3

            Console.WriteLine("Задание 2.3");
            double y3;
            double x43;
            Console.WriteLine();
            Console.Write("Введите значение для X: ");
            double x3 = double.Parse(Console.ReadLine());
            Console.WriteLine();
            x43 = x3;
            Console.Write("Введите ограничение для функции Y(X): ");
            double a1 = double.Parse(Console.ReadLine());
            Console.WriteLine();
            Console.WriteLine("Значение X до обработки - {0}", x3);
            block3(ref x3,ref a1);
            y3 = x3;
            Console.WriteLine();
            Console.WriteLine("Значение функции Y(X) после обработки - {0}", y3);
            Console.WriteLine();
            Console.WriteLine();

            //Num 4

            Console.WriteLine("Задание 2.4");
            double y4;
            double x44;
            Console.WriteLine();
            Console.Write("Введите значение для X: ");
            double x4 = double.Parse(Console.ReadLine());
            Console.WriteLine();
            x44 = x4;
            Console.WriteLine("Значение X до обработки - {0}", x4);
            block4(ref x4);
            y4 = x4;
            Console.WriteLine();
            Console.WriteLine("Значение функции Y(X) после обработки - {0}", y4);
            Console.WriteLine();
            Console.WriteLine();

            Console.WriteLine("ТАБЛИЦА ЗНАЧЕНИЯ ФУНКЦИИ Y(X)");
            Console.ForegroundColor = ConsoleColor.Red;
            Console.WriteLine("* Значение функции y(x) с учётом заданного промежутка равного - [X>=0.9] or [0<=X<0.9] or [X<0], при введенном значении X = {0}, Y(X) = {1} *", x41, y1);
            Console.WriteLine("* Значение функции y(x) с учётом заданного промежутка равного - [|X|<3] or [3<=|X|<9] or [|x|>=9], при введенном значении X = {0}, Y(X) = {1} *", x42, y2);
            Console.WriteLine("* Значение функции y(x) с учётом заданного промежутка равного - [X<{0}] or [X>{0}] or [X={0}], при введенном значении X = {1}, Y(X) = {2} *",a1, x43, y3);
            Console.WriteLine("* Значение функции y(x) с учётом заданного промежутка равного - [|X|<0.1] or [0.1<|X|<=0.2] or [|X|>0.2], при введенном значении X = {0}, Y(X) = {1} *", x44, y4);
            Console.ResetColor();
            Console.ReadKey();

            //BLOCK 3's























































        }

        



         

    }
}
