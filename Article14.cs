﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Text.RegularExpressions;

namespace oop3
{
    class Article : Edition
    {
        string name = " ";
        string SecondAuthorName = " ";
        string NameofMagazine = " ";
        string Number = " ";
        string yearofborn = " ";

        public Article(string name, string SecondAuthorName, string NameofMagazine, string Number, string yearofborn)
        {
            this.name = name;
            this.SecondAuthorName = SecondAuthorName;
            this.NameofMagazine = NameofMagazine;
            this.Number = Number;
            this.yearofborn = yearofborn;
        }

        public override void EditionInfo()
        {
            Console.WriteLine("Название статьи: " + name);
            Console.WriteLine("Фамилия автора: " + SecondAuthorName);
            Console.WriteLine("Название журнала: " + NameofMagazine);
            Console.WriteLine("Номер журнала: " + Number);
            Console.WriteLine("Год издания: " + yearofborn);
        }

        public override string Needed()
        {
            return SecondAuthorName;
        }
    }
}
