﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace dicks
{
    class Student : IComparable
    {
        public string Name { get; private set; }
        public int YearOfBirth { get; private set; }
        public string HomeAddress { get; private set; }
        public string School { get; private set; }

        public Student(string name, int yearOfBirth, string homeAddress, string school)
        {
            Name = name;
            YearOfBirth = yearOfBirth;
            HomeAddress = homeAddress;
            School = school;
        }

        

        #region IComparable Members

        public int CompareTo(object obj)
        {
            return YearOfBirth.CompareTo(((Student)obj).YearOfBirth);
        }

        #endregion

        public override string ToString()
        {
            return string.Format("{0}; {1}; {2}; {3}",
                                 Name, YearOfBirth, HomeAddress, School);
        }


    }
}

