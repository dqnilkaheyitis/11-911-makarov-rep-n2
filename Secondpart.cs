﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SecondPart
{
    class Program
    {
        static void Main(string[] args)
        {
            RacionalFraction fraction = new RacionalFraction(12, 8);
            RacionalFraction fraction1 = new RacionalFraction(13, 5);

            //Ex.1
            fraction.reduce();

            //Ex.2
            fraction.add(fraction1);

            //Ex.3
            fraction.add(fraction1);

            //Ex.4
            fraction.sub(fraction1);

            //Ex.5
            fraction.sub1(fraction1);

            //Ex.6
            fraction.multi(fraction1);

            //Ex.7
            fraction.multi1(fraction1);

            //Ex.8
            fraction.div(fraction1);

            //Ex.9
            fraction.div1(fraction1);

            //Ex.10
            Console.WriteLine(fraction.ToString());

            //Ex.11
            Console.WriteLine(fraction.value());

            //Ex.12
            if (fraction.equals(fraction1) == true)
                Console.WriteLine("Первая дробь больше второй.");
            else
                Console.WriteLine("Вторая дробь больше первой");

            //Ex.13
            Console.WriteLine(fraction.numberPart());
        }
    }
}
