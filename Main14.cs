﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace oop3
{
    class Program
    {
        static void Main(string[] args)
        {
            //Ex.1
            
            Figure[] a = new Figure[3];
            a[0] = new Rectangle(2, 5);
            a[1]= new Triangle(2, 3, 4);
            a[2] = new Circle(6);
            for (int i = 0; i < a.Length; i++)
            {
                Console.WriteLine("{0}; Площадь и Периметр: {1} ", a[i].Perm(), a[i].FigureInfo());
            }
            Console.WriteLine();
            

            //Ex.2
            
            Console.Write("Введите X: ");
            double x = double.Parse(Console.ReadLine());
            Console.Write("Введите a: ");
            double a1 = int.Parse(Console.ReadLine());
            Console.Write("Введите b: ");
            double b = int.Parse(Console.ReadLine());
            Console.Write("Введите c: ");
            double c = int.Parse(Console.ReadLine());
            Function[] b1 = new Function[3];
            b1[0] = new Kub(a1, b, c, x);
            b1[1] = new Hyperbola(a1, b, x);
            b1[2] = new Line(a1, b, x);
            for (int i = 0; i < b1.Length; i++)
            {
                Console.WriteLine(b1[i].NameFunc());
            }
            Console.WriteLine();
            

            //Ex.3
            
           
            
            int[] w = new int[3];
            Edition[] e = new Edition[3];
            e[0] = new Book("Дом", "Пушкин", "2001", "Москва");
            e[1] = new Article("Новости!", "Пушкин", "Магазин", "201", "2008");
            e[2] = new InternetResourse("Youtube", "PewDiePie", "youtube.com", "annotations");
            for (int i = 0; i < e.Length; i++)
            {
                e[i].EditionInfo();
                Console.WriteLine();
            }
            Console.Write("Введите фамилию автора: ");
            string search = Console.ReadLine();
            char s1 = ' ';
            char s2 = ' ';
            int h = 0;
            int j1 = 0;
            for (int i = 0; i < e.Length; i++)
            {
                if (e[i].Needed().Length == search.Length-1)
                {
                    j1 = 0;
                    for (int j = 0; j < search.Length-1; j++)
                    {
                        s1 = search[j];
                        s2 = e[i].Needed()[j];
                        if (s1 == s2)
                        {
                            j1++;
                        }
                        if (j1 == search.Length-1)
                        {
                            Console.WriteLine();
                            e[i].EditionInfo();
                            h++;
                            Console.WriteLine();
                        }
                    }
                }
            }
            if (h == 0)
            {
                Console.WriteLine("Изданий с указаным автором нет в наличии.");
            }
            
        }
    }
}
