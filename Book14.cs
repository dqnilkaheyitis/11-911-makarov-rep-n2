﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Text.RegularExpressions;

namespace oop3
{
    class Book : Edition
    {
        string name = " ";
        string SecondNameAuthor = " ";
        string yearofborn = " ";
        string Edition = " ";

        public Book(string name, string SecondNameAuthor, string yearofborn, string Edition)
        {
            this.name = name;
            this.SecondNameAuthor = SecondNameAuthor;
            this.yearofborn = yearofborn;
            this.Edition = Edition;
        }

        public override void EditionInfo()
        {
            Console.WriteLine("Название книги: " + name);
            Console.WriteLine("Фамилия автора: " + SecondNameAuthor);
            Console.WriteLine("Год издания: " + yearofborn);
            Console.WriteLine("Издание: " + Edition);
        }

        public override string Needed()
        {
            return SecondNameAuthor;
        }
    }
}
