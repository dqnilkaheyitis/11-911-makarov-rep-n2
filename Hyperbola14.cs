﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace oop3
{
    class Hyperbola : Function
    {
        public double a;
        public double b;
        public double x;

        public Hyperbola(double a, double b, double x)
        {
            this.a = a;
            this.b = b;
            this.x = x;
        }


        public override double Func()
        {
            return a / x + b;
        }

        public override string NameFunc()
        {
            string s = "Функция a/x + b: " + Func();
            return s;
        }
    }
}
