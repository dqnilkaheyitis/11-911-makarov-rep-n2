﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;

namespace laba13
{ 
    class RationVector2D 
    {
        public RacionalFraction x;
        public RacionalFraction y;

        public RationVector2D()
        {
            x = new RacionalFraction();
            y = new RacionalFraction();
        }

        public RationVector2D (RacionalFraction x, RacionalFraction y)
        {
            this.x = x;
            this.y = y;
        }

        public RationVector2D add(RationVector2D obj)
        {
            RacionalFraction x1 = x.add(obj.x);
            RacionalFraction y1 = y.add(obj.y);
            RationVector2D a = new RationVector2D(x1, y1);
            return a;
        }

        public string String()
        {
            return (x.ToString() + ";" + y.ToString());
        }

        public double lenght()
        {
            
            return x.lenght();
        }

        public RacionalFraction scalarProduct(RationVector2D obj)
        {
            double s = x.ScalarProduct(obj.x);
            double s1 = y.ScalarProduct(obj.y);
            double s2 = s + s1; 
            RacionalFraction a = new RacionalFraction(s2,1);
            return a;
        }

        public bool equals(RationVector2D obj)
        {
            if (x.lenght() > obj.x.lenght())
            {
                return true;
            }
            else
                return false;
        }


    }
}
