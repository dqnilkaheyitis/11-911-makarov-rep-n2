﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Text.RegularExpressions;

namespace laba11
{
    class Example
    {
        static void first(string s)
        {
            Regex regex = new Regex(@"\d{3}-\d{3}|\d{2}-\d{2}-\d{2}|\d{3}-\d{2}-\d{2}");
            MatchCollection mathes = regex.Matches(s);
            if (mathes.Count > 0)
                foreach (Match bob in mathes)
                    Console.WriteLine(bob.Value);
            else
                Console.WriteLine("Таких номеров нет в наличии.");
        }

        static void sec(string s)
        {
            Regex regex = new Regex(@"([10-31]\.[10-12]\.[1900-2010]{4})|([0]*[1-9]\.[0]*[1-9]\.[1900-2010]{4})");
            MatchCollection mathes = regex.Matches(s);
            if (mathes.Count > 0)
                foreach (Match bob in mathes)
                    Console.WriteLine(bob.Value);
            else
                Console.WriteLine("Заданных дат нет.");
        }

        static void the(string s)
        {
            Regex regex = new Regex (@"(25[0-5]|2[0-4][0-9]|[0-1]?[0-9][0-9]?)(\.(25[0-5]|2[0-4][0-9]|[0-1]?[0-9][0-9]?)){3}"); 
            MatchCollection matches = regex.Matches(s);
            if (matches.Count > 0)
            {
                foreach (Match bob in matches)
                    Console.WriteLine("Действительный IP - адресс: " + bob.Value);
            }
            else
                Console.WriteLine("Заданных ip-адрессов не существует.");
            Console.Write("Введите число, если последняя триада ip-адресса будет начинается с этой цифрой, то такой ip-адресс будет удалён: ");
            int x = int.Parse(Console.ReadLine());
            Regex regex1 = new Regex(@"(25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)(\.(25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?))(25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)(\.(25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?))(\.("+x.ToString()+ @"[0-5]|" + x.ToString() + @"[0-4][0-9]|[01]?[" + x.ToString() + @"][0-9]?))");
            string t = regex1.Replace(s, string.Empty);
            MatchCollection matches1 = regex.Matches(t);
            if (matches1.Count > 0)
            {
                foreach (Match bob1 in matches1)
                    Console.WriteLine("Остался: " + bob1.Value);
            }
            else
                Console.WriteLine("Все адресса были удаленны.");
        }
        //(www\.)*|(http://)*|(https:/)*
        //+\.org|\.ru|\.net|.com 

        static void four(string s)
        {
            Regex regex = new Regex(@"((www\.)*|(https:\\)*|(http:\\)*)\w+(\.org|\.net|\.com|\.ru|\.onion)");
            MatchCollection baba1= regex.Matches(s);
            if (baba1.Count > 0)
            {
                foreach (Match bib in baba1)
                    Console.WriteLine("Действительный web-сайт: " + bib.Value);
            }
            else
                Console.WriteLine("Нет дейстаительных web-сайтов.");

        }
        static void Main(string[] args)
        {
            string s = Console.ReadLine();
            four(s);

            Console.WriteLine("Задание 1.");
            Console.Write("Введите строку: ");
            s = Console.ReadLine();
            first(s);
            Console.WriteLine();

            Console.WriteLine("Задание 2.");
            Console.Write("Введите даты: ");
            s = Console.ReadLine();
            sec(s);
            Console.WriteLine();

            Console.WriteLine("Задание 3.");
            Console.Write("Введите IP-адресс: ");
            s = Console.ReadLine();
            the(s);
            Console.WriteLine();

            Console.WriteLine("Задание 4.");
            Console.Write("Введите web-сайты: ");


            Console.ReadKey();
        }
    }
}
