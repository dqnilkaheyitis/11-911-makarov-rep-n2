﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace oop
{
    class Vector2D
    {
        public double x;
        public double y;

        public Vector2D()
        {

        }

        public Vector2D(double x, double y)
        {
            this.x = x;
            this.y = y;
        }

        public Vector2D add(Vector2D obj)
        {
            Vector2D a =  new Vector2D(x+obj.x, y+obj.y);
            return a; 
        }
        
        public void add2(Vector2D obj)
        {
           this.x = x + obj.x;
            this.y = y + obj.y;
        }

        public Vector2D sub(Vector2D obj)
        {
            Vector2D b = new Vector2D(x - obj.x, y - obj.y);
            return b;
        }

        public void sub2(Vector2D obj)
        {
            this.x = x - obj.x;
            this.y = y - obj.y;
        }
        
        public Vector2D multi(double d)
        {
            this.x = x * 2.0;
            this.y = y * 2.0;
            Vector2D c = new Vector2D(x, y);
            return c;
        }

        public void multi2(double d)
        {
            this.x = x * d;
            this.y = y * d;
            Vector2D c = new Vector2D(x, y);
        }

        public override string ToString()
        {
            string s = "(" + x + " : " + y + ")";
            return s;
        }
        
        public double lenght()
        {
            double z = (Math.Sqrt(Math.Pow(x, 2) + Math.Pow(y, 2)));
            return z;
        }

        public void ScalarProduct(Vector2D obj)
        {
            Console.WriteLine("Вектор 1 с координатами - " + obj.x + ":" + obj.y);
            Console.WriteLine("Вектор 2 с координатами - " + x + ":" + y);
            double sc1 = x * obj.x + y * obj.y;
            Console.WriteLine("Скалярное произведение векторов - " + sc1);
        }

        public void cos(Vector2D obj)
        {
            double sq = Math.Sqrt(Math.Pow(x, 2) + Math.Pow(y, 2)) * Math.Sqrt(Math.Pow(obj.x, 2) + Math.Pow(obj.y, 2));
            Console.WriteLine("Угол между двумя векторами - " + Math.Cos(sq));
        }

        public bool equals(Vector2D obj)
        {
            double a = (Math.Sqrt(Math.Pow(x, 2) + Math.Pow(y, 2)));
            double b = (Math.Sqrt(Math.Pow(obj.x, 2) + Math.Pow(obj.y, 2)));
            Console.WriteLine("Длина первого вектора - " + a);
            Console.WriteLine("Длина второго вектора - " + b);
            if (a > b)
                return true;
            else
                return false;
        }

    }
}
