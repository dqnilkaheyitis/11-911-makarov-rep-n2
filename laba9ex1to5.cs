﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace laba9
{
    class Program
    {
        static StringBuilder Go(string[] w, StringBuilder s1)
        {
            Console.Write("Введите необходимое количество символов в слове: ");
            int n = int.Parse(Console.ReadLine());
            string s2 = " ";
            for (int i = 0; i < w.Length; i++)
            {
                s2 = w[i];
                if (s2.Length <= n)
                {
                    s1.Append(s2 + " ");
                }
            }
            return  s1;
        }

        static StringBuilder Ho(string[] w, StringBuilder s3)
        {
            string x = " ";

            for (int i = 0; i < w.Length; i++)
            {
                x = w[i];
                for (int j = 0; j < x.Length; j++)
                {
                    if (char.IsUpper(x[j]))
                    {
                        s3.Append(x + " ");
                    }
                }
            }
            return s3;
        }

        static StringBuilder Do(string[] w, StringBuilder s4)
        {
            string x = " ";
            int c = 0;
            string c1 = " ";
            for (int i = 0; i < w.Length; i++)
            {
                x = w[i];
                {
                    for (int j = i+1; j < w.Length; j++)
                    {
                        if (x == w[j])
                            w[j] = " ";
                    }
                }
            }
            for (int i = 0; i < w.Length; i++) 
            {
                if (w[i] != " ")
                {
                    s4.Append(w[i] + " ");
                }
            }
            return s4;
        }
        static int How(string[] w1, int count3)
        {
            Console.Write("Введите слово: ");
            string r = Console.ReadLine();
            string x = " ";
            int d = 0;
            for (int i = 0; i < w1.Length; i++)
            {
                x = w1[i];
                d = 0;
                for (int j = 0; j < x.Length; j++)
                {
                    if (r.Length == x.Length && r[j] == x[j])
                        d++;
                }
                if (d == x.Length)
                {
                    count3++;
                }
            }
            return count3;
        }

        static string Max(string [] w, string p)
        {
            int max = 0;
            for (int i = 0; i < w.Length; i++)
            {
                if (w[i].Length > max)
                {
                    max = w[i].Length;
                    p = w[i];
                }
            }
            return p;
        }
        static void Main(string[] args)
        {
            Console.WriteLine("Задание 1.");
            string s = Console.ReadLine();
            StringBuilder s1 = new StringBuilder();
            string [] w = s.Split();
            Console.WriteLine("Обновленное сообщение: {0}", Go(w, s1));
            Console.WriteLine();

            Console.WriteLine("Задание 2.");
            StringBuilder s3 = new StringBuilder();
            Console.WriteLine("Только прописные: " + Ho(w, s3));
            Console.WriteLine();

            Console.WriteLine("Задане 3.");
            StringBuilder s4 = new StringBuilder();
            Console.WriteLine("Обновленная строка: " + Do(w, s4));
            Console.WriteLine();

            Console.WriteLine("Задание 4.");
            int count3 = 0;
            Console.Write("Введите новую строку: ");
            string s_1 = Console.ReadLine();                                       
            string[] w1 = s_1.Split();
            Console.WriteLine("Слово встречается {0} раз.", How(w1, count3));
            Console.WriteLine();

            Console.WriteLine("Задание 5.");
            string p = " ";
            Max(w, p);
            Console.WriteLine("Самое длинное слово - " + Max(w, p));


            Console.ReadKey();
        }
    }
}
