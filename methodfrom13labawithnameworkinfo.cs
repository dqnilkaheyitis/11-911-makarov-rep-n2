﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace laba132st
{
    class worker 
    {
        public int ok { get; set; }
        public int prem { get; set; }
        public int work_count { get; set; }

        public worker()
        {

        }

        public worker(int ok, int prem, int work_count) 
        {
            this.ok = ok;
            this.prem = prem;
            this.work_count = work_count;
        }

        bool CheckCount()
        {
            if (work_count > 100)
            {
                return false;
            }
            else
                return true;    
        }

        public void ad()
        {
            if (CheckCount() == false)
            {
                Console.WriteLine("Проверьте правильность введеных данных для пункта Стаж Работы! ");
                Environment.Exit(0);
            }
        }

        public virtual void Table()
        {
            Console.WriteLine("Оклад: " + ok);
            Console.WriteLine("% Премии: " + prem);
            Console.WriteLine("Стаж работы: " + work_count + " года.");
        }

        public double Cash()
        {
            Up();
            double y =  ok + ok * prem / 100;
            return y;
        }
        
        public virtual double Tax()
        {
            Up();
            double x = Cash() / 100 * 13;
            return x;
        }

        public virtual double Handed()
        {
            Up();
            double x = Cash() - Tax();
            return x;
        }

        public void Up()
        {
            if (work_count > 10)
            {
                prem *= 2;
            }
        }

    }
}
