﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace laba7
{


    class Program
    {
        static void First(int[,] quack)
        {
            Console.Write("Введите номер строки, которую необходимо вывыести: ");
            int k = int.Parse(Console.ReadLine());
            k = k - 1;
            for (int i = k; i == k; i++)
            {
                for (int j = 0; j < 4; j++)
                {
                    Console.Write("{0}", quack[i, j]);
                }
            }
        }

        static void Second(int[,] quack)
        {
            Console.Write("Введите номер столбца, который необходимо вывести: ");
            int k = int.Parse(Console.ReadLine());
            k = k - 1;
            for (int i = 0; i < 3; i++)
            {
                for (int j = k; j == k; j++)
                {
                    Console.Write("{0}", quack[i, j]);
                    Console.WriteLine();
                }
            }
        }

        static void Sum(int[,] quack)
        {
            int sum = 0;
            for (int i = 0; i < quack.GetLength(0); i++)
                for (int j = 0; j < quack.GetLength(1); j++)
                {
                    if (i == 2)
                    {
                        sum += quack[i, j];
                    }
                }
            Console.Write("Суммма элементов 3-ей строки равна: ");
            Console.WriteLine("{0}", sum);
        }




        static void Main(string[] args)
        {
            Console.WriteLine("Задание 1.");
            Console.WriteLine();
            int[,] quack = new int[3, 4];
            Console.WriteLine("Введите массив: ");

            for (int i = 0; i < quack.GetLength(0); i++)
            {
                for (int j = 0; j < quack.GetLength(1); j++)
                {
                    Console.Write("Значение для {0} - ой строки, {1} - го столбца: ", i + 1, j + 1);
                    quack[i, j] = int.Parse(Console.ReadLine());
                    Console.WriteLine();
                }
            }
            Console.WriteLine();
            Console.WriteLine("Полный массив: ");
            for (int i = 0; i < quack.GetLength(0); i++)
            {
                for (int j = 0; j < quack.GetLength(1); j++)
                {
                    Console.Write(quack[i, j] + " ");
                    if (j == 4)
                        Console.WriteLine();
                }
                Console.WriteLine();
            }
            First(quack);
            Console.WriteLine();
            Second(quack);
            Console.WriteLine();
            Sum(quack);
            Console.WriteLine();
            Console.WriteLine();


            Console.WriteLine("Задание 2.");
            Console.WriteLine();
            Console.WriteLine("Полный массив: ");
            for (int i = 0; i < quack.GetLength(0); i++)
            {
                for (int j = 0; j < quack.GetLength(1); j++)
                {
                    Console.Write(quack[i, j] + " ");

                    if (j == 3)
                        Console.WriteLine();
                }
            }
            Console.WriteLine();
            Console.WriteLine("Изменённый массив: ");
            int temp = 0;
            for (int i = 0; i < quack.GetLength(0) / 2; i++)
            {
                for (int j = 0; j < quack.GetLength(1); j++)
                {
                    temp = quack[i, j];
                    quack[i, j] = quack[quack.GetLength(0) - i - 1, j];
                    quack[quack.GetLength(1) - i - 2, j] = temp;
                }
            }
            for (int i = 0; i < quack.GetLength(0); i++)
            {
                for (int j = 0; j < quack.GetLength(1); j++)
                {
                    Console.Write(quack[i, j] + " ");
                    if (j == 3)
                    {
                        Console.WriteLine();
                    }
                }
            }
            Console.WriteLine();
            Console.WriteLine();


            Console.WriteLine("Задание 3.");
            int[,] vagoon = new int[18, 36];
            Random hello = new Random();
            int k = 0;
            for (int i = 0; i < vagoon.GetLength(0); i++)
            {
                for (int j = 0; j < vagoon.GetLength(1); j++)
                {
                    vagoon[i, j] = hello.Next(0, 2);
                }
            }
            for (int i = 0; i < vagoon.GetLength(0); i++)
            {
                for (int j = 0; j < vagoon.GetLength(1); j++)
                {
                    if (vagoon[i, j] == 0)
                        Console.WriteLine("Место под номером {0}, в вагоне под номером {1} явяется свободным для покупки.", j + 1, i + 1);
                    else 
                        Console.WriteLine("Место под номером {0}, в вагоне под номером {1} явяется купленным.", j + 1, i + 1);
                    Console.WriteLine();
                }
            }
            Console.WriteLine();
            int l = 0;
            l = hello.Next(0, 17);
            for (int i = l; i == l; i++) 
                for (int j = 0; j < vagoon.GetLength(1); j++)
                {
                    if (vagoon[i, j] == 0)
                    {
                        k++;
                    }
                }
            Console.WriteLine("В вагоне под номером {0}, {1} свободным мест.",l,k);
            Console.WriteLine();
            Console.WriteLine();

            Console.WriteLine("Задание 4.");
            int[,] fourex = new int[4, 6];
            Random ranex = new Random();
            int xmin = 0;
            int xmin1 = 1000;
            int ymin1 = 1000;
            int ymin = 0;
            for (int i = 0; i < fourex.GetLength(0); i++)
            {
                for (int j = 0; j < fourex.GetLength(1); j++)
                {
                    fourex[i, j] = ranex.Next(0, 10);
                }
            }
            Console.WriteLine("Полный массив:");
            for (int i = 0; i < fourex.GetLength(0); i++)
            {
                for (int j = 0; j < fourex.GetLength(1); j++)
                {
                    Console.Write(fourex[i, j] + " ");
                    if (j == 5)
                        Console.WriteLine();
                }
            }

            for (int i = 0; i < fourex.GetLength(0); i++)
            {
                for (int j = 0; j < fourex.GetLength(1); j++)
                {
                    xmin += fourex[i, j];
                    if (j == 5)
                    {
                        if (xmin < xmin1)
                            xmin1 = xmin;
                        xmin = 0;
                    }
                }
            }
            for (int j = 0; j < fourex.GetLength(1); j++)
            {
                for (int i = 0; i < fourex.GetLength(0); i++)
                {
                    ymin += fourex[i, j];
                    if (i == 3)
                    {
                        if (ymin < ymin1)
                            ymin1 = ymin;
                        ymin = 0;
                    }
                }
            }

            Console.WriteLine();
            Console.WriteLine("Наименьшая сумма строки - {0}", xmin1);
            Console.WriteLine();
            Console.WriteLine("Наименьшая сумма столбца - {0}", ymin1);
            Console.WriteLine();
            Console.WriteLine();


            Console.WriteLine("Задание 5.");
            Console.WriteLine();
            int[,] fivevo = new int[5, 5];
            Random ranvevo = new Random();
            int x = 0;
            int x_count = 0;
            int x1 = 0;
            int y = 4;
            int y_count = 0;
            for (int i = 0; i < fivevo.GetLength(0); i++)
                for (int j = 0; j < fivevo.GetLength(1); j++)
                {
                    fivevo[i, j] = ranvevo.Next(10);
                }

            Console.WriteLine("Полный массив:");
            for (int i = 0; i < fivevo.GetLength(0); i++)
            {
                for (int j = 0; j < fivevo.GetLength(1); j++)
                {
                    Console.Write(fivevo[i, j] + " ");
                    if (j == 4)
                        Console.WriteLine();
                }
            }
            Console.WriteLine();

            for (int i = 0; i < fivevo.GetLength(0); i++)
                for (int j = 0; j < fivevo.GetLength(1); j++)
                {
                    if (j == x && x1 == i)
                    {
                        x_count += fivevo[i, j];
                        x++;
                        x1++;
                    }
                }
            for (int i = 0; i < fivevo.GetLength(0); i++)
                for (int j = 0; j < fivevo.GetLength(1); j++)
                {
                    if (j == y)
                    {
                        if (fivevo[i, j] % 2 == 0 && fivevo[i, j] != 0)
                        {
                            y_count++;
                        }
                        y--;
                    }

                }
            Console.WriteLine("Сумма элементов главной диагонали - {0}", x_count);
            Console.WriteLine("Количество чётных элементов побочной диагонали - {0}", y_count);
            Console.WriteLine();
            Console.WriteLine();


            Console.WriteLine("Задание 6.");
            Console.WriteLine();
            int w = 0;
            int[] solo = new int[5];
            Random ranolo = new Random();
            for (int i = 0; i < solo.Length; i++)
            {
                solo[i] = ranolo.Next(10);
            }
            Console.WriteLine("Полный двумерный массив:");
            for (int i = 0; i < fivevo.GetLength(0); i++)
            {
                for (int j = 0; j < fivevo.GetLength(1); j++)
                {
                    Console.Write(fivevo[i, j] + " ");
                    if (j == 4)
                        Console.WriteLine();
                }
            }

            Console.WriteLine();
            Console.Write("Введите необходимый номер столбца двумерного массива, сумма чётных элементов которого заменят элементы одномерного массива:" + " ");
            int q = int.Parse(Console.ReadLine());
            Console.WriteLine();
            for (int j = q-1; j<fivevo.GetLength(1); j++)
                for (int i = 0; i < fivevo.GetLength(0); i++)
                {
                    if (fivevo[i, j] % 2 == 0 && fivevo[i, j] != 0 && j == q-1)
                        w += fivevo[i, j];
                }
            Console.WriteLine("Одномерный массив до изменений:");
            for (int i = 0; i < solo.Length; i++)
            {
                Console.Write(solo[i] + " ");
            }
            Console.WriteLine();
            Console.WriteLine();
            Console.WriteLine("Число, на которое будет замены все элементы одномерного массива - {0}.", w);
            Console.WriteLine();
            for (int i = 0; i < solo.Length; i++)
            {
                solo[i] = w;
            }
            Console.WriteLine("Изменённый одномерный массив:");
            for (int i = 0; i < solo.Length; i++)
            {
                Console.Write(solo[i] + " ");
            }
            Console.WriteLine();
            Console.WriteLine();
            int[] hard = new int[5];
            Random haha = new Random();
            for (int i = 0; i < hard.Length; i++)
            {
                hard[i] = haha.Next(10);
            }
            Console.WriteLine("Одномерный массив №2 до изменений:");
            for (int i = 0; i < hard.Length; i++)
            {
                Console.Write(hard[i] + " ");
            }
            Console.WriteLine();
            Console.WriteLine();
            Console.WriteLine("Полный двумерный массив:");
            for (int i = 0; i < fivevo.GetLength(0); i++)
            {
                for (int j = 0; j < fivevo.GetLength(1); j++)
                {
                    Console.Write(fivevo[i, j] + " ");
                    if (j == 4)
                        Console.WriteLine();
                }
            }
            Console.WriteLine();
            Console.Write("Введите номер строки, наибольший из элементов в которой будет заменять все элементы одномерного массива:" + " " );
            Console.WriteLine();
            int f = int.Parse(Console.ReadLine());
            int fmax = 0;
            for (int i = f-1; i < fivevo.GetLength(0); i++)
            {
                for (int j = 0; j < fivevo.GetLength(1); j++)
                {
                    if (i == f - 1)
                    {
                        if (fivevo[i, j] > fmax)
                            fmax = Math.Abs(fivevo[i, j]);
                    }
                }
            }
            for (int i = 0; i < hard.Length; i++)
            {
                hard[i] = fmax;
            }
            Console.WriteLine();
            Console.WriteLine("Измененный одномерный массив: ");
            for (int i = 0; i < hard.Length; i++)
            {
                Console.Write(hard[i] + " ");
            }












            







                    Console.ReadKey();
        }
    }
}
        



