﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Matrix
{
    class Matrix2x2
    {

        public double[,] m = new double[2, 2];



        public Matrix2x2()
        {

        }

        public Matrix2x2(double a)
        {
            m[0, 0] = a;
            m[0, 1] = a;
            m[1, 0] = a;
            m[1, 1] = a;
        }

        public Matrix2x2(double a, double b)
        {
            m[0, 0] = a;
            m[0, 1] = b;
            m[1, 0] = a;
            m[1, 1] = b;
        }


        public void Print()
        {
            for (int i = 0; i < m.GetLength(0); i++)
                for (int j = 0; j < m.GetLength(1); j++)
                {
                    Console.Write(m[i, j] + " ");
                    if (j == 1)
                        Console.WriteLine();
                }
        }


        public Matrix2x2 add(Matrix2x2 obj)
        {
            Matrix2x2 temp = new Matrix2x2();

            for (int i = 0; i < m.GetLength(0); i++)
                for (int j = 0; j < m.GetLength(1); j++)
                {
                    temp.m[i, j] = m[i, j] + obj.m[i, j];
                }
            return temp;
        }

        public void add2(Matrix2x2 obj)
        {
            for (int i = 0; i < m.GetLength(0); i++)
                for (int j = 0; j < m.GetLength(1); j++)
                {
                    m[i, j] = m[i, j] + obj.m[i, j];
                    Console.Write(m[i, j] + " ");
                    if (j == 1)
                        Console.WriteLine();
                }

        }

        public Matrix2x2 sub(Matrix2x2 obj)
        {
            Matrix2x2 temp = new Matrix2x2();

            for (int i = 0; i < m.GetLength(0); i++)
                for (int j = 0; j < m.GetLength(1); j++)
                {
                    temp.m[i, j] = m[i, j] - obj.m[i, j];
                }
            return temp;
        }

        public void sub2(Matrix2x2 obj)
        {
            Matrix2x2 temp = new Matrix2x2();

            for (int i = 0; i < m.GetLength(0); i++)
                for (int j = 0; j < m.GetLength(1); j++)
                {
                    m[i, j] = m[i, j] - obj.m[i, j];
                    Console.Write(m[i, j] + " ");
                    if (j == 1)
                        Console.WriteLine();
                }
        }

        public Matrix2x2 multNumber(double x)
        {
            Matrix2x2 temp = new Matrix2x2();

            for (int i = 0; i < m.GetLength(0); i++)
                for (int j = 0; j < m.GetLength(1); j++)
                {
                    temp.m[i, j] = x * m[i, j];
                }
            return temp;
        }


        public void multNumber2(double x)
        {
            Matrix2x2 temp = new Matrix2x2();

            for (int i = 0; i < m.GetLength(0); i++)
                for (int j = 0; j < m.GetLength(1); j++)
                {
                    m[i, j] = x * m[i, j];
                    Console.Write(m[i, j] + " ");
                    if (j == 1)
                        Console.WriteLine();
                }
        }

        public Matrix2x2 multi(Matrix2x2 obj)
        {
            Matrix2x2 temp = new Matrix2x2();
            temp.m[0, 0] = m[0, 0] * obj.m[0, 0] + m[0, 1] * obj.m[1, 0];

            temp.m[0, 1] = m[0, 0] * obj.m[0, 1] + m[0, 1] * obj.m[1, 1];

            temp.m[1, 0] = m[1, 0] * obj.m[0, 0] + m[1, 1] * obj.m[1, 0];

            temp.m[1, 1] = m[1, 0] * obj.m[0, 1] + m[1, 1] * obj.m[1, 1];

            return temp;
        }

        public void multi2(Matrix2x2 obj)
        {
            Matrix2x2 temp = new Matrix2x2();
            m[0, 0] = m[0, 0] * obj.m[0, 0] + m[0, 1] * obj.m[1, 0];

            m[0, 1] = m[0, 0] * obj.m[0, 1] + m[0, 1] * obj.m[1, 1];

            m[1, 0] = m[1, 0] * obj.m[0, 0] + m[1, 1] * obj.m[1, 0];

            m[1, 1] = m[1, 0] * obj.m[0, 1] + m[1, 1] * obj.m[1, 1];
            for (int i = 0; i < m.GetLength(0); i++)
                for (int j = 0; j < m.GetLength(1); j++)
                {
                    Console.Write(m[i, j] + " ");
                    if (j == 1)
                        Console.WriteLine();
                }
        }

        public double det()
            
        {
            return m[0, 0] * m[1, 1] - m[0, 1] * m[1, 0];
        }

        public void Transpon()
        {
            double x_1_0 = m[1, 0];
            double x_0_1 = m[0, 1];
            m[1, 0] = x_0_1;
            m[0, 1] = x_1_0;
            for (int i = 0; i < m.GetLength(0); i++)
                for (int j = 0; j < m.GetLength(1); j++)
                {
                    Console.Write(m[i, j] + " ");
                    if (j == 1)
                        Console.WriteLine();
                }
        }

        public Matrix2x2 inverseMatrix()
        {
            Console.WriteLine("Провередем изначальную проверку на то, что определитель не является нулем. Допустим имеется транспонированная матрица.");
            Console.WriteLine();
            Matrix2x2 mata = new Matrix2x2();
            try
            {
                Console.WriteLine(1 / det()); 
            }
            finally
            {
                Console.WriteLine("Ошибка! Определитель равен нулю.");
                mata = multNumber(0);
            }
            return mata;
        }


        public Matrix2x2 equivalentDiagonal()
        {
            Matrix2x2 temp = new Matrix2x2();
            temp.m[0, 1] = 0;
            temp.m[1, 0] = 0;
            return temp;
        }



    }
}
