﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace laba16
{
    interface INumber
    {
        long q1 { get; set; }
        INumber add(INumber n);
        
        INumber sub(INumber n);
        
    }
}
