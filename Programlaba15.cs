﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace laba15
{
    class Program
    {
        static void Main(string[] args)
        {
            string s = "                                                                      Добро пожаловать в <ThreeSideOFWord> переводчик имени Макарова Д.В.";
            Console.ForegroundColor = ConsoleColor.Blue;
            Console.WriteLine("{0, 50}", s);
            Console.ResetColor();


            Console.WriteLine();
            Console.WriteLine();
            Console.ForegroundColor = ConsoleColor.Red;
            Console.WriteLine("ПРИМЕЧАНИЕ: Для правильной работы приложения, все слова необходимо вводить с маленькой буквы! ");
            Console.ResetColor();
            Console.WriteLine();
            Console.WriteLine();


            Eng eng0 = new Eng();

            if (eng0.Correct() == 1)
            {
                Console.Write("Введите слово: ");
                string s1 = Console.ReadLine();
                Eng eng1 = new Eng(s1);
                Tat tat1 = new Tat(s1);
                eng1.Translater();
                tat1.Translater();
                eng1.Thank();
            }
            else
            {
                Console.Write("Введите слово: ");
                string s1 = Console.ReadLine();
                Console.Write("Введите слово: ");
                string s2 = Console.ReadLine();
                Eng eng2 = new Eng(s1, s2);
                Tat tat2 = new Tat(s1, s2);
                tat2.Translater();
                eng2.Translater();
                tat2.Thank();
            }

            Console.ReadKey();
        }
    }
}
