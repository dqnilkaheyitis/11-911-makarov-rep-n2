﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Text.RegularExpressions;
namespace oop3
{
    class InternetResourse : Edition
    {
        string nameint = " ";
        string SecondAuthorName = " ";
        string link = " ";
        string annotations = " ";

        public InternetResourse(string nameint, string SecondAuthorName, string link, string annotations)
        {
            this.nameint = nameint;
            this.SecondAuthorName = SecondAuthorName;
            this.link = link;
            this.annotations = annotations;
        }

        public override void EditionInfo()
        {
            Console.WriteLine("Название ресурса: " + nameint);
            Console.WriteLine("Фамилия автора: " + SecondAuthorName);
            Console.WriteLine("Ссылка: " + link);
            Console.WriteLine("Аннотации: " + annotations);
        }

        public override string Needed()
        {
            return SecondAuthorName;
        }

    }
}
