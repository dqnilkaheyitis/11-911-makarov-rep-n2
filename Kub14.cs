﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace oop3
{
    class Kub : Function
    {
        public double a;
        public double b;
        public double x;
        public double c;
        public Kub()
        {

        }


        public Kub(double a, double b, double c, double x)
        {
            this.a = a;
            this.b = b;
            this.x = x;
            this.c = c;
        }

        public override double Func()
        {
            return a * Math.Pow(x,2) + b * x + c;
        }

        public override string NameFunc()
        {
            string s = "Функция ax^2+bc+c: " + Func();
            return s;
        }
    }
}
